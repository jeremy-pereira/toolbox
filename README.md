#  Toolbox
This framework provides a number of utilities and type extensions that I find are generally useful.

As of release 15.0.1, this is a Swift package.

## Logger
This class provides basic logging functionality. At the moment, it has the capability to log at different levels and there is a hierarchical structure of loggers. Loggers are identified by a string name with hierarchical levels being denoted by dots e.g.

```
"Toolbox.Logger"
```

You can set the logging level for any logger identified by its name and for all loggers in the hierarchy below it. e.g.

```Swift
Logger.setLevel(.debug, forName: "Toolbox.Logger")
```
Also, each logger has a stack of levels associated with it so you can push a new level temporarily.

```Swift
private static log = Logger.getLogger("Toolbox.Logger")
func foo(f: Bool)
{
    Logger.pushLevel(.debug, forName: "Toolbox.Logger")
    defer { Logger.popLevel(forName: "Toolbox.Logger") }
    
    log.debug("Will be printed")
    if f 
    {
    	Logger.pushLevel(.info, forName: "Toolbox.Logger")
       defer { Logger.popLevel(forName: "Toolbox.Logger") }
       log.debug("Will not be printed")
    }
    log.debug("Will be printed")
}  
```

## Date
A set of functions to allow platform independent date manipulation. Since the Swift foundation date is effectively available everywhere, this protocol is more or less redundant.

## Monad
Extensions to some types to provide Monad behaviour for those types. Unfortunately, as of Swift 4, it is impossible to define a single generic protocol that defines a Monad because of the lack of support of higher kinded types. However, we can do it piecemeal.

Essentially for a monad, we have a type M which provides a context (or "wrapper") for other types (a simple example is `Optional` which can provide a context for any other type). Then we have two functions:

- `unit` is a function that takes an object of some type `T` and returns the monad wrapping it. For `Optional` the constructor provides this function. In Swift, it's type is `(T) -> M<T>` 
- `bind` is a function that takes a monadic value of type `M<T>`, unwraps it and uses the unwrapped value as the argument to a function that produces another monadic value of type `M<U>`. The function's type is `(T) -> M<U>` which means the type of `bind` is `(M<T>, (T) -> M<U>) -> M<U>`. `bind` gives us the ability to easily chain successive operations together that take values of types and produce monads of types, for example, functions of type `(T) -> Optional<U>` are common in Swift. 

We define an operator `>>-` (comes from the Runes library) which is the same as `bind` to make things look nicer when written down. Thus, if you have three functions 

```
func f(_ x: T) -> U?
func g(_ x: U) -> V?
func h(_ x: V) -> W?
```

if we want to apply one after the other, we can use Swift unwrapping

    if let x1 = f(x)
    {
        if let x2 = g(x1)
        {
            if let x3 = h(x2)
            {
                // Do something with x3
            }
        }
    }

but this gets unweildy after a few chainings. With the `bind` operator it becomes

    if let x3 = f(x) >>- g >>- h
    {
        // Do something
    }

What exactly `bind` and `unit` do is implementation dependent. However, they must follow some rules.
 - `unit` must be neutral i.e. `unit(v) >>- f` is equivalent to `f(v)` and `m >>- unit` is equivalent to `m`.
 - `bind` must be associative i.e. `m >>- f >>- g` is equivalent to `m >>- { x in f(x) >>- g }` 

## Shunting Yard

This is an implementation of the shunting yard algorithm for turning infix expressions into
reverse Polish expressions to make them easier to execute.

See https://en.wikipedia.org/wiki/Shunting-yard_algorithm for a full description.

## Geometry Extensions

### NSSize

`NSSize` can be multiplied or divided by a `CGFloat`.

## IO Streams

These are designed to look a lot like the Java `InputStream` and its implementors. 

### InStream

This is a protocol that models the Java `InputStream` protocol. I call it `InStream` because I do not want to confuse it with the already existing `Foundaion.InputStream`.

### Markable

Adoptable by input streams that can reset themselves to a location. The stream can mark a position and then later return to it. Not all streams will be able to do this, of course.

### IO.Error

Some of the errors that can be thrown by streams. Most thrown errors are underlying OS errors, so there won't be many of these.

## InStream

This is as interface that defines an input stream. It is designed to look like a Java `InputStream`. Some implementations are provided.

### ArrayInStream

An `InStream` whose backing store is an array of bytes.

### FileInStream

A stream from a file. It uses the operating system calls to do IO and thus provides no buffering beyond what the operating system gives you.


## New for 20
Converted the `FileInStream` to use the new Swift System package. 

## New for 19
### IO Streams
There is a new protocol `InStream` that is designed to look like a Java `InputStream`. 
## New for 18.x

There are some extensions for the NS geometry types e.g. `NSSize`, `NSPoint` etc. These make it easier to manipulate them as a whole e.g. multiply an `NSSize` by a constant.

A new function for sequences called `reduceWithStop(into:_:)` has been define dthat  allows you to do a reduce but stop before the end (18.1).

`KeyOrderedArray` has been removed.

## New for 17.5.0

`BinarySearchMap` is a new data structure that behabes a lot like a dictionary except that its keys are comparable and values are found by doing a binary search. 

## New for 17.1.0

`OrderedArray` has been tidied up. 

- Superfluous conformance to `Sequence` is gone.
- `OrderedArray` now conforms to `ExpressibleByArrayLiteral` if its elements are comparable and `Key == `Element`. You might need to declare it something like this:

  ```
  let testValues: OrderedArray<Int, Int> = [2, 1, 4, 3]
  ```
- new functions have been added to make it easier to find indexes and elements. These are `first(withKey:)` and `index(ofKey:)`
 
## Changes for 17.0.0

All of the previously marked deprecated features are gone.

## New for 16.1.0

`OrderedArray` has been tidied up. 

- Superfluous conformance to `Sequence` is gone.
- `OrderedArray` now conforms to `ExpressibleByArrayLiteral` if its elements are comparable and `Key == `Element`. You might need to declare it something like this:

  ```
  let testValues: OrderedArray<Int, Int> = [2, 1, 4, 3]
  ```
- new functions have been added to make it easier to find indexes and elements. These are `first(withKey:)` and `index(ofKey:)`
 

## New for 16.0.0

### Logger

`Logger` now accepts a list of objects to which log messages are written. Any object can be added to the list as long as it conforms to `TextOutputStream`. For example:

```Swift

extension Array: TextOutputStream where Element == String
{
	public mutating func write(_ string: String)
	{
		self.append(string)
	}
}

let log = Logger.getLogger("Foo.Bar")
log.outputStreams = [Array<String>()]
log.level = .debug
log.debug("DEBUG YES")
// log.outputStreams[0][0] == "DEBUG YES"
```

`Logger` also now has members that can be used as instances of `TextOutputStream`

```Swift
let log = Logger.getLogger("Foo.Bar")
log.outputStreams = [Array<String>()]
log.level = .debug
print("Hello", to: &log.debugStream)
```

By default logging now defaults to `.error` instead of `.none`

### MutableCollection

[`MutableCollection`](https://developer.apple.com/documentation/swift/mutablecollection) has an extension to allow you to mutate each element in place.

```Swift
public mutating func mutateEach(block: (inout Element) throws -> ()) rethrows
```
For eample:

```Swift
var foo = [1, 2, 3]
foo.mutateEach { $0 += 1 }
// foo == [2, 3, 4]
```

### Failing functions

`notImplemented()` and `mustBeOverriden()` have been introduced to act as markers that cause a fatal error. The former can be used anywhere you have some code that needs to be implemented but shouldn't be allowed to slip through undetected. The latter can be used for abstract functions that need to be overridden in subclasses.

