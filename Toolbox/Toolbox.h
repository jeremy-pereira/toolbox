//
//  Toolbox.h
//  Toolbox
//
//  Created by Jeremy Pereira on 15/01/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Toolbox.
FOUNDATION_EXPORT double ToolboxVersionNumber;

//! Project version string for Toolbox.
FOUNDATION_EXPORT const unsigned char ToolboxVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Toolbox/PublicHeader.h>


