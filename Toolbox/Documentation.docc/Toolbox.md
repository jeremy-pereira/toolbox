# ``Toolbox``

A compendium of useful types, extensions and functions.

## Overview

This is a collection of various different types and functions that I use in many of my projects. It arose originally because of some (what I considered) missing functionality in the Swift standard library. 

## Topics

### Logging

A simple class to make logging a bit easier. 

- ``Logger``
- ``HasLogging``

### IO

Types and protocols to make IO a bit easier, or a bit more Java-like.

- ``InStream``
- ``Markable``
- ``Pollable``
- ``PollEvent``
- ``ArrayInStream``
- ``FileInStream``
- ``Closeable``

### Random Numbers

A type erased random number generator

- ``AnyRNG``

### State Machines

Abstraction of a finite state automaton

- ``StateMachine``
- ``StateType``
- ``TransitionShould``

### Shunting Yard

An implementation of the shunting yard algorithm.

- ``ShuntingYard``
- ``Shuntable``

### Data Structures

- ``BinarySearchMap``
- ``Stack``
- ``OrderedArray``
- ``CircularIterator``

