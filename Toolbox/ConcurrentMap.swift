//
//  ConcurrentMap.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 18/04/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
// The concurrent map indication comes from here:
// https://talk.objc.io/episodes/S01E90-concurrent-map
//
import Foundation

/// A thread safe wrapper for an arbitrary type
///
/// This works by synchronising all operations of the wrapped object using a
/// serial queue,
public class ThreadSafe<A>
{
	private var _value: A
	private let queue = DispatchQueue(label: "ThreadSafe")


	/// Wrap an object in the thread safe versipn
	///
	/// - Parameter value: The value to wrap.
	init(_ value: A)
	{
		self._value = value
	}


	/// Return the wrapped object in a thread safe way.
	var value: A
	{
		return queue.sync { _value }
	}

	/// Perfrom an operation on the wtrapped object in a thread safe way.
	///
	/// The block is performed in the context of the queue used to synchronise
	/// all operations on the wrapped object.
	/// - Parameter transform: Things to do to the object.
	func atomically(_ transform: (inout A) -> ())
	{
		queue.sync
			{
				transform(&self._value)
		}
	}
}

public extension Array
{
	/// A concurrent map operation on the elements of an array.
	///
	/// - Parameter transform: The map function.
	/// - Returns: An array of the mapped objects.
	func concurrentMap<B>(_ transform: @escaping (Element) -> B) -> [B]
	{
		let result = ThreadSafe(Array<B?>(repeating: nil, count: count))
		DispatchQueue.concurrentPerform(iterations: count)
		{
			idx in
			let element = self[idx]
			let transformed = transform(element)
			result.atomically {
				$0[idx] = transformed
			}
		}
		return result.value.map { $0! }
	}
}
