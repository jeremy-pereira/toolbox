//
//  FileInStream.swift
//  
//
//  Created by Jeremy Pereira on 10/04/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import SystemPackage
import Foundation

private let log = Logger.getLogger("Toolbox.FileInStream")

/// A file input stream
///
/// This class encapsulates a file on the file system. It provides no extra
/// buffering over and above the operating system calls. On *nix these are the
/// standard Posix system calls.
open class FileInStream: InStream
{
	private var fileDescriptor: FileDescriptor?
	private let url: URL

	public init(url: URL) throws
	{
		guard url.isFileURL else { throw IO.Error.isNotFileUrl(url) }
		self.url = url
		let path = FilePath(url.path)
		let fileDescriptor = try FileDescriptor.open(path, .readOnly)
		self.fileDescriptor = fileDescriptor
	}

	deinit
	{
		self.closeQuietly { log.error("\($0)") }
	}

	open func available() throws -> Int
	{
		guard let fileDescriptor = fileDescriptor else { throw IO.Error.fileNotOpen(url) }

		let currentPosition = try fileDescriptor.seek(offset: 0, from: .current)
		let endPosition = try fileDescriptor.seek(offset: 0, from: .end)
		try fileDescriptor.seek(offset: currentPosition, from: .start)

		return Int(endPosition - currentPosition)
	}

	open func read(bytes: inout Data, offset: Int, length: Int) throws -> Int
	{
		guard let fileDescriptor = fileDescriptor else { throw IO.Error.fileNotOpen(url) }
		try sanitise(offset: offset, length: length, bufferCount: bytes.count)
		let bytesRead: Int = try bytes.withUnsafeMutableBytes
		{
			(bufferPointer) -> Int in
			let regionToCopy = UnsafeMutableRawBufferPointer(rebasing: bufferPointer[offset ..< (offset + length)])

			return try fileDescriptor.read(into: regionToCopy)
		}
		guard bytesRead != -1 else { throw IO.Error.io(url, errno) }
		return bytesRead == 0 ? -1 : bytesRead
	}

	open func read() throws -> UInt8?
	{
		var data = Data(count: 1)
		let readCount = try read(bytes: &data, offset: 0, length: 1)
		guard readCount == 1 else { return nil }
		return data[0]
	}


	/// Block size for reading chunks for `readAllBytes()`
	let chunkSize = 4096

	open func readAllBytes() throws -> Data
	{
		var ret = Data()
		var buffer = Data(count: chunkSize)
		var bytesRead = try read(bytes: &buffer, offset: 0, length: buffer.count)
		while bytesRead != -1
		{
			let usedBuffer = buffer.dropLast(buffer.count - bytesRead)
			ret.append(usedBuffer)
			bytesRead = try read(bytes: &buffer, offset: 0, length: buffer.count)
		}
		return ret
	}

	open func close() throws
	{
		guard let fileDescriptor = fileDescriptor else { return }
		self.fileDescriptor = nil
		try fileDescriptor.close()
	}
}
