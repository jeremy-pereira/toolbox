//
//  Pollable.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 21/05/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//
import Foundation
#if os(macOS) || os(iOS) || os(tvOS) || os(watchOS)
private let nativePoll = Darwin.poll
#elseif os(Linux)
private let nativePoll = Glibc.poll
#endif

private let log = Logger.getLogger("Toolbox.Pollable")

/// Option set for the different poll events
public struct PollEvent: OptionSet
{
    public init(rawValue: Int16)
    {
        self.rawValue = rawValue
    }

    public init(_ cValue: Int32)
    {
        self.rawValue = Int16(cValue)
    }

    public let rawValue: Int16

    /// Returned if the socket is in error
    public static let PollErr = PollEvent(POLLERR)
    /// Returned if the device has been disconnected
    public static let PollHup = PollEvent(POLLHUP)
    /// Data other than high priority data may be read without blocking
    public static let PollIn = PollEvent(POLLIN)
    /// Returned if the socket is not an open socket
    public static let PollNVal = PollEvent(POLLNVAL)
    /// Normal data can be written without blocking
    public static let PollOut = PollEvent(POLLOUT)
    /// High priority data may be read without blocking
    public static let PollPri = PollEvent(POLLPRI)
    /// Out of band data may be read without blocking
    public static let PollRdBand = PollEvent(POLLRDBAND)
    ///Normal data may be read without blocking
    public static let PollRdNorm = PollEvent(POLLRDNORM)
    /// Out of band data may be written without blocking
    public static let PollWrBand = PollEvent(POLLWRBAND)
    ///Normal data may be written without blocking
    public static let PollWrNorm = PollEvent(POLLWRNORM)
}

/// This extension  allows us to iterate through the set bits of a poll event.
extension PollEvent: Sequence
{
    public struct Iterator: IteratorProtocol
    {
        public typealias Element = PollEvent
        fileprivate var currentBit: Int16
        fileprivate var parentEvent: PollEvent

        public mutating func next() -> PollEvent?
        {
            var ret: PollEvent?
            let bits = parentEvent.rawValue
            while ret == nil && currentBit >= 0
            {
                if bits & (1 << currentBit) != 0
                {
                    ret = PollEvent(rawValue: bits & (1 << currentBit))
                }
                currentBit -= 1
            }
            return ret
        }
    }

    public func makeIterator() -> PollEvent.Iterator
    {
        return Iterator(currentBit: 15, parentEvent: self)
    }
}

/// PRotocol that marks a socket as being one we can use with `Socket.poll`
public protocol Pollable
{
    ///
    /// Poll the given sockets to find one that is ready to read or write
    /// without blocking.  The delgate event method is then called
    ///
    /// - Parameter sockets: List of sockets to poll
    /// - Parameter timeOut: Time out to wait for in milliseconds
    /// - Returns: The sockets that are ready for data.  The array may be empty
    ///            if the timeout expired.
    /// - Throws: If the system call failed for any reason.
    ///
    static func poll(_ sockets: [Pollable], timeOut: Int) throws

    /// A pollable object must have a Unix file descriptor associated.
    var rawSocket: Int32 { get }

    /// We need to be able to close a pollable if some error occurs.
    func close()

    /// The set of epoll events we are interested in on this object
    var pollEvents: PollEvent { get }

    /// Function to call if a poll event occurred on the pollable object.
    /// - Parameter events: The events that occurred
    /// - Throws: The fiunction may throw an error, if it does, the pollable object
    ///           will be closed.
    func pollEventOccurred(_ events: PollEvent) throws
}

public extension Pollable
{
	static func poll(_ sockets: [Pollable], timeOut: Int) throws
    {
        var pollFDs = [pollfd](repeating: pollfd(), count: sockets.count)
        for i in 0 ..< sockets.count
        {
            pollFDs[i].fd = sockets[i].rawSocket
            pollFDs[i].events = sockets[i].pollEvents.rawValue
        }

        let fdCount = nativePoll(&pollFDs, nfds_t(pollFDs.count), Int32(timeOut))
        if fdCount == -1
        {
            throw ToolboxError.osError(errno: errno, message: "Failed poll")
        }
        if fdCount > 0
        {
            for i in 0 ..< sockets.count
            {
                if pollFDs[i].revents != 0
                {
                    do
                    {
                        try sockets[i].pollEventOccurred(PollEvent(rawValue: pollFDs[i].revents))
                    }
                    catch
                    {
                        log.error("Error on socket \(sockets[i]), \(error)")
                        sockets[i].close()
                    }
                }
            }
        }
    }
}
