//
//  Date.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 15/10/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import Foundation

/// Return the current date and time.
public func now() -> Date
{
    return Date()
}

/// Create a date from a Unix style number of seconds since Jan 1 1970
/// - Parameter unixTime: Number of seconds since Jan 1st 1970
public func dateFrom(unixTime: Int) -> Date
{
    return Foundation.Date(timeIntervalSince1970: TimeInterval(unixTime))
}

public func dateFrom(isoString: String) -> Date?
{
    return Foundation.Date.isoDateFormatter.date(from: isoString)
}

private extension Foundation.Date
{

    static var httpDateFormatter: DateFormatter =
    {
        let ret = DateFormatter()
        ret.dateFormat = "EEE, dd MMM yyyy HH:mm:ss 'GMT'"
        ret.timeZone = TimeZone(abbreviation: "GMT")
        return ret
    }()

    static var isoDateFormatter: DateFormatter =
    {
        let ret = DateFormatter()
        ret.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        ret.timeZone = TimeZone(abbreviation: "GMT")
        return ret
    }()

}

extension Date
{
    public var httpString: String
    {
        return Foundation.Date.httpDateFormatter.string(from: self)
    }

    public var isoString: String
    {
        return Foundation.Date.isoDateFormatter.string(from: self)
    }

    public var asNSDate: Foundation.Date
    {
		return self
    }
}
