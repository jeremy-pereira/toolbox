//
//  Logger.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 23/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import Foundation
private var myLoggers = [ "" : Logger() ]

/// A logger class
///
/// Obtain a logger with the getLogger factory method.
///
/// This class provides basic logging functionality. At the moment, it has the capability to log at different levels and there is a hierarchical structure of loggers. Loggers are identified by a string name with hierarchical levels being denoted by dots e.g.
///
/// ```
/// "Toolbox.Logger"
/// ```
///
/// You can set the logging level for any logger identified by its name and for all loggers in the hierarchy below it. e.g.
///
/// ```Swift
/// Logger.setLevel(.debug, forName: "Toolbox.Logger")
/// ```
/// Also, each logger has a stack of levels associated with it so you can push a new level temporarily.
///
/// ```Swift
/// private static log = Logger.getLogger("Toolbox.Logger")
/// func foo(f: Bool)
/// {
///     Logger.pushLevel(.debug, forName: "Toolbox.Logger")
///     defer { Logger.popLevel(forName: "Toolbox.Logger") }
///
///     log.debug("Will be printed")
///     if f
///     {
///     	Logger.pushLevel(.info, forName: "Toolbox.Logger")
///        defer { Logger.popLevel(forName: "Toolbox.Logger") }
///        log.debug("Will not be printed")
///     }
///     log.debug("Will be printed")
/// }
/// ```
open class Logger
{
    /// Log levels
    public enum Level: Int, CaseIterable
    {
        case none = -1
        case emergency = 0
        case alert
        case critical
        case error
        case warning
        case notice
        case info
        case debug
        case trace
    }

    fileprivate var parent: Logger?

	/// List of output streams that the logger sends log messages to.
	///
	/// If you want to log to a new destination, you can add an instance of a type
	/// that conformsa to `TextOutputStream` here.
	public var outputStreams: [TextOutputStream]

	private var inputStreams: [Level : TextStream] = [:]

	fileprivate init(parent: Logger? = nil,
					 outputStreams: [TextOutputStream] = [FileHandle.standardError])
    {
        self.parent = parent
		self.outputStreams = outputStreams

		inputStreams = Dictionary(uniqueKeysWithValues: Level.allCases.map{ ($0, TextStream(log: self, level: $0)) })

		if parent == nil
		{
			// This is the root logger
			_level = .error
		}
    }

    fileprivate var _level: Level?

    /// The log level of this logger.
	///
	/// If you don't set the logging level explicitly, the level will default to
	/// the same level as its parent logger. The root logger defaults to `.error`
    open var level: Level
    {
		get
        {
            let ret: Level

            if let _level = _level
            {
                ret = _level
            }
            else if let parent = parent
            {
                ret = parent.level
            }
            else
            {
                ret = .none
            }
            return ret
        }

        set
        {
			_level = newValue
        }
    }

    fileprivate var levelStack = Stack<Level>()

	/// Sets the level and returns `self`
	///
	/// Use this to set the level in a declaration of a logger. Fore example:
	///  ```
	///   private let log = Logger.getLogger("biosim-swift.Simulation").with(level: .debug)
	///  ```
	/// - Parameter level: The log level to set
	/// - Returns: `self`
	open func with(level: Level) -> Logger
	{
		self.level = level
		return self
	}

    /// Set the log level put save the old level on a stack so we can retrieve
    /// it later.
    /// - Parameter newLevel: new level for the logger
    open func pushLevel(_ newLevel: Level)
    {
		levelStack.push(level)
        level = newLevel
    }

    /// Retrieve a previously saved log level.
	///
	/// If no level was saved on the stack,
    /// the level becomes whatever it would be if it had not been set explicitly. 
    /// i.e. the level is inherited from its parent.
    open func popLevel()
    {
        if let restoredLevel = levelStack.pop()
        {
            level = restoredLevel
        }
        else
        {
            _level = nil
        }
    }

    /// True if logging is at debug level or more detailed.
    open var isDebug: Bool { return level.rawValue >= Level.debug.rawValue }

    /// Log a message at the requested level.
    ///
    /// Nothing will be logged if the requested level is of lower priority than
    /// the logger's level.
    /// 
    /// - Parameter message: The message to log
    /// - Parameter requestedLevel: The level to log at
	/// - Parameter separator: separator to be added to each log message,
	///                        defaults  to `\n`
    ///
	open func log(_ message: @autoclosure () -> String, atLevel requestedLevel: Level, separator: String = "\n")
    {
		if requestedLevel.rawValue <= level.rawValue
        {
			print(message(), to: &inputStreams[requestedLevel]!)
        }
    }

	/// Log an error
	///
	/// - Todo: have a separator parameter so we can override `\n` as the separator.
	/// - Parameter message: The message to log
    open func error(_ message: @autoclosure () -> String)
    {
		log(message(), atLevel: Level.error)
    }

	/// Log a warning
	///
	/// - Todo: have a separator parameter so we can override `\n` as the separator.
	/// - Parameter message: The message to log
    open func warn(_ message: @autoclosure () -> String)
    {
		log(message(), atLevel: Level.warning)
    }

	/// Log an informational message
	///
	/// - Todo: have a separator parameter so we can override `\n` as the separator.
	/// - Parameter message: The message to log
    open func info(_ message: @autoclosure () -> String)
    {
		log(message(), atLevel: Level.info)
    }
	/// Log a debug message
	///
	/// - Todo: have a separator parameter so we can override `\n` as the separator.
	/// - Parameter message: The message to log

    open func debug(_ message: @autoclosure () -> String)
    {
		log(message(), atLevel: Level.debug)
    }
	/// Log a trace message
	///
	/// - Todo: have a separator parameter so we can override `\n` as the separator.
	/// - Parameter message: The message to log
    open func trace(_ message: @autoclosure () -> String)
    {
		log(message(), atLevel: Level.trace)
    }


    ///
    /// Get the logger with the given name. 
    ///
    /// A hierarchhical naming convention applies.  The separator for name parts
    /// is a "."  If we can't find the name, we chop the last bit off and 
    /// repeat until we get to the root logger.
    ///
    /// - Parameter loggerName: . separated hierarchical log name.
    /// - Returns: The best logger match  for the name.
    public static func getLogger(_ loggerName: String) -> Logger
    {
        var ret: Logger

        if let candidate = myLoggers[loggerName]
        {
            ret = candidate
        }
        else
        {
            ret = Logger(parent: getLogger(loggerName.withoutLastComponent))
            myLoggers[loggerName] = ret
        }
        return ret
    }

    ///
    /// Set the log level for the given name.
    ///
    /// Sets the logging level for the logger with the given name and, by extension,
    /// all sub loggers that do not already have explicit levels.
    ///
    /// - Parameter level: The log level
    /// - Parameter forName: Name of the logger to set the level for.
	@available(*, deprecated, renamed: "set(level:forName:)")
    public static func setLevel(_ level: Logger.Level, forName: String)
    {
		set(level: level, forName: forName)
    }

	/// Set the log level for the given name
    ///
    /// Sets the logging level for the logger with the given name and, by extension,
    /// all sub loggers that do not already have explicit levels.
	///
	/// - Parameters:
	///   - level: The log level
	///   - name: The name of the logger for which to set the level
	public static func set(level: Logger.Level, forName name: String)
	{
		if let theLogger = myLoggers[name]
        {
            theLogger.level = level
        }
        else
        {
            let newLogger = Logger()
            newLogger.level = level
            myLoggers[name] = newLogger
        }
	}
    ///
    /// Push the log level for the given name.
    ///
    /// Sets the logging level for the logger with the given name and, by extension,
    /// all sub loggers that do not already have explicit levels. The previous
    /// level is saved on a stack, si it can be restored later.
    ///
    /// - Parameter level: The log level
    /// - Parameter forName: Name of the logger to set the level for.
    public static func pushLevel(_ level: Logger.Level, forName: String)
    {
        if let theLogger = myLoggers[forName]
        {
            theLogger.pushLevel(level)
        }
        else
        {
            let newLogger = Logger()
            newLogger.pushLevel(level)
            myLoggers[forName] = newLogger
        }
    }

    /// Pop the level for the given logger.
	///
	/// Discards the current log level and
    /// restores the previous log level. If you pop when the level has never 
    /// been saved for this logger, the level defaults to the parent logger's 
    /// level. If you pop when the level has never been explicitly set for this
    /// logger, nothing happens.
    /// - Parameter loggerName: The logger to pop the level for.
    public static func popLevel(forName loggerName: String)
    {
        if let theLogger = myLoggers[loggerName]
        {
            theLogger.popLevel()
        }
    }

	/// A text output stream for trace level events
	open var traceStream: TextStream
	{
		get { inputStreams[.trace]! }
		set {}
	}
	/// A text output stream for debug level events
	open var debugStream: TextStream
	{
		get { inputStreams[.debug]! }
		set {}
	}
	/// A text output stream for info level events
	open var infoStream: TextStream
	{
		get { inputStreams[.info]! }
		set {}
	}
	/// A text output stream for notice level events
	open var noticeStream: TextStream
	{
		get { inputStreams[.notice]! }
		set {}
	}
	/// A text output stream for warning level events
	open var warnStream: TextStream
	{
		get { inputStreams[.warning]! }
		set {}
	}
	/// A text output stream for error level events
	open var errorStream: TextStream
	{
		get { inputStreams[.error]! }
		set {}
	}
	/// A text output stream for critical lewvel events
	open var criticalStream: TextStream
	{
		get { inputStreams[.critical]! }
		set {}
	}
	/// A text output stream for alert level events
	open var alertStream: TextStream
	{
		get { inputStreams[.alert]! }
		set {}
	}
	/// a text output stream for emergency events
	open var emergencyStream: TextStream
	{
		get { inputStreams[.emergency]! }
		set {}
	}



	/// A text output stream that filters messages by log level.
	///
	/// You can use this the same as any other `TextOutputStream` but if it's
	/// log level is higher than the log level of its owning log, it will
	/// discard the output.
	public struct TextStream: TextOutputStream
	{
		/// The level at which this stream is writing data.
		public let level: Level
		/// Use this to test if the stream is currently discarding output
		public var isDiscarding: Bool { return level.rawValue > log.level.rawValue }

		// This is fine because loggers never go away. They are all kept in the
		// myLoggers dictionary
		weak private var log: Logger!

		fileprivate init(log: Logger, level: Level)
		{
			self.level = level
			self.log = log
		}

		public mutating func write(_ string: String)
		{
			if !isDiscarding
			{
				log.outputStreams.mutateEach { $0.write(string) }
			}
		}
	}
}

extension String
{

    /// This string but without the last "dot ectension".
    var withoutLastComponent: String
    {
        var index = self.endIndex
        var foundIndex: String.Index?
        let start = self.startIndex
        while foundIndex == nil && index != start
        {
            index = self.index(before: index)
            if self[index] == "."
            {
                foundIndex = index
            }
        }
        let ret: String
		if let foundIndex = foundIndex
        {
			let remainingChars = self[start ..< foundIndex]
            ret = String(remainingChars)
        }
        else
        {
            ret = ""
        }
        return ret
    }
}


/// Protocol that signals a type has a logger.
///
/// This protocol exposes the type's logger externally so that other code can
/// use it. IT's primarily designed for protocol extensions so default functions
/// can use the logger associated with the type ratrher than the logger for the
/// extension.
public protocol HasLogging
{
	/// The type's logger.
	static var log: Logger { get }
}
