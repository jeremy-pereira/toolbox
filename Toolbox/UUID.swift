//
//  UUID.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 23/11/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//
//  Copyright (c) Jeremy Pereira 2015-2023
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

///    Encapsulates a UUID
///
@available(*, deprecated, message: "Use Foundation UUID instead")
public struct UUID: CustomStringConvertible, Hashable
{

	/// The bytes of the UUID
    public let bytes: [UInt8]
	/// Create a new unique UUID
    public init()
    {
        let foundationUUID = Foundation.UUID()
        var foundationBytes: [UInt8] = [UInt8](repeating: 0, count: 16)
        (foundationUUID as NSUUID).getBytes(&foundationBytes)
        bytes = foundationBytes
    }

	/// The UUID as a string
	///
	/// Formatted as:
	///
	/// `xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx`
    public var description: String
    {
        var ret: String = ""
		for i in 0 ..< self.bytes.count
        {
			if i == 4 || i == 6 || i == 8 || i == 10
            {
                ret += "-"
            }
            ret += self.bytes[i].hexString
        }
        return ret
    }
}
