//
//  StateMachine.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 21/06/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//

import Foundation

///    Protocol for a type describing all the states of a state machine and the rules for
///    transitioning between them.
public protocol StateType
{
    /// This function applies the rules for transitioning between two states.
    ///
    /// - parameter to: The new state to transition to
    ///
    /// - returns: An enumeration value that tells the state machine if it can
    ///            transition to a new state or not.
    func canTransition(to: Self) -> TransitionShould<Self>
}

/// Result of canTransition
///
/// - carryOn: fire the action and carry on
/// - abort:    leave in the same state, run failure handler
/// - redirect: carry on but redirect to the given state
public enum TransitionShould<T>
{
    /// fire the action and carry on
    case carryOn
    /// leave in the same state, run failure handler
    case abort
    /// carry on but redirect to the given associated state
    case redirect(T)
}

/// Encapsulates a state machine. This separates the allowed types of transition
/// from the actions performed on a transition.
///
/// To use, define a `YourState` type that conforms to `StateType` implementing the
/// function `canTransition()` that gives the rules for transitioning between states.
/// Then create an instance of a `StateMachine<YourStateType>` with an initial
/// state. Then in a loop or whatever, call `transition()` to transition between
/// states.
open class StateMachine<State: StateType>
{
    /// Typealias that describes the type of an action function.
    public typealias Action = (_ from: State, _ to: State) throws -> ()

	public private(set) var aborted = false

    /// The current state of the machine.
    open fileprivate(set) var state: State
    fileprivate var inTransition = false

    /// Try to transition betwen states. If the transition is allowed, it
    /// happens and the action is called. If the transition
    /// is not allowed, it doesn't happen and the `onFailure` event handler is
    /// called.
    ///
    /// - parameter newState: The new state to transition to
    /// - parameter action:   An action to be performed. It happens just
    ///                       before the state property changes. It is a
    ///                       runtime error to try to change the state in an 
    ///                       action.
    ///
    /// - throws: Errors thrown by the action will be passed through.
	/// - Returns: `true` if the new state is the one that was asked for
    @discardableResult
	open func transition(to newState: State, action: Action) rethrows -> Bool
    {
		guard !aborted else { fatalError("Cannot transition after state machine has aborted") }
        guard !inTransition else { nestedTransitionError(self) ; return false }
        inTransition = true
        defer { inTransition = false }
		let ret: Bool
        switch state.canTransition(to: newState)
        {
        case .carryOn:
            try action(state, newState)
            state = newState
			ret = true
        case .abort:
            onFailure(state, newState)
			aborted = true
			ret = false
        case .redirect(let redirectState):
            try action(state, redirectState)
            state = redirectState
			ret = false
        }
		return ret
    }

    /// Try to transition between states. If the transitioni s not allowed,
    /// it doesn't happen and the `onFailure` event handler is
    /// called.
    ///
    /// - parameter newState: The state to transition to.
	/// - Returns: `true` if the new state is the one that was asked for
    @discardableResult
    open func transition(to newState: State) -> Bool
    {
        guard !inTransition else { nestedTransitionError(self) ; return false }
		let ret: Bool
        switch state.canTransition(to: newState)
        {
        case .carryOn:
            state = newState
			ret = true
        case .abort:
            onFailure(state, newState)
			ret = false
        case .redirect(let redirectState):
            state = redirectState
			ret = false
        }
		return ret
    }

    /// Event handler for state transition failures.
    open var onFailure: (_ from: State, _ to: State) -> () = { _, _ in }

    /// A closure that handles nested transitions. This is here to allow testing
    /// without causing a fatalError. It should not be altered in normal operation.
    internal var nestedTransitionError: (StateMachine) -> ()
        = { _ in fatalError("Detected attempted transition whilst in a transition action") }

    /// Create a state machine and initialise it to the given start state.
    ///
    /// - parameter startState: The state the machine starts in.
    ///
    /// - returns: A new state machine
    public init(startState: State)
    {
		self.state = startState
    }
}
