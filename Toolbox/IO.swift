//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 21/03/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Foundation

/// Any type that needs to be closed when you finish using it
public protocol Closeable
{
	/// Clost the instance of the type
	/// Throws: Implementations may throw if something goes wrong
	func close() throws
}

extension Closeable
{
	/// Close the input stream, supressing any errors
	///
	/// Since closing is allowed to throw, it can be a bit annoying, if you
	/// don't really care - for example, if you are closing in a `defer`.
	/// - Parameter failAction: Something to do if an error is thrown while
	///                         closing. by default the error will be thrown away
	public func closeQuietly(failAction: ((Error) -> ()) = { _ in })
	{
		do
		{
			try self.close()
		}
		catch
		{
			failAction(error)
		}
	}
}


/// Interface that defines an input stream.
///
/// This is designed to look like a Java `InputStream`
public protocol InStream: Closeable
{
	/// An estimate of the amount of bytes that can be read without blocking
	/// - Returns: The count of available bytes. The actual number of bytes
	///            might be more than reported.
	/// - Throws: If there's an error.
	func available() throws -> Int

	/// Read's the next byte
	///
	/// - Returns: The next byte or `nil` if we are at the end of the file.
	/// - Throws: If some sort of error occurs on the input stream
	func read() throws -> UInt8?

	/// Read some bytes into a `Data` object.
	///
	/// The maximum number of bytes read is determined by the size of the byte
	/// buffer.
	///
	/// By default this function is a wrapper for
	/// ```
	/// return try read(bytes: &bytes, offset: 0, length: bytes.count)
	/// ```
	/// - Parameter bytes: A byte buffer to read into.
	/// - Returns: The number of bytes actually read or `-1` if the
	///            end of the stream has been read.
	/// - Throws: If some sort of error occurs while reading,
	func read(bytes: inout Data) throws -> Int

	/// Read some bytes into the buffer at a given offset for a given maximum
	/// length.
	///
	/// - Parameter bytes: The buffer to read data into
	/// - Parameter offset: The offset in the buffer to start putting the bytes at
	/// - Parameter length: The maximum number of bytes to read.
	/// - Returns: -1 if the end of the stream has been reasched or the number
	///            of bytes actually read.
	/// - Throws: if the offset is negative, the length is negative or
	///           `offset + length > bytes.count`, also if an IO error occurs.
	func read(bytes: inout Data, offset: Int, length: Int) throws -> Int

	/// Read all the remaining bytes on the stream.
	///
	/// - Returns: A data object containing all the remaining bytes
	/// - Throws: If there is an IO error.
	func readAllBytes() throws -> Data
}

extension InStream
{
	public func read(bytes: inout Data) throws -> Int
	{
		return try read(bytes: &bytes, offset: 0, length: bytes.count)
	}

	/// A helper function to sanitise parameters for `read(bytes:offset:length)`
	///
	/// If this function executes without throwing, it is guaranteed that
	/// `offset` and `length` define a contiguous set of bytes that do not
	/// overflow the buffer at either end.
	/// - Parameters:
	///   - offset: The offset of the start of the byte sequence in the buffer
	///   - length: The length of the byte sequence
	///   - bufferCount: The number of bytes in the buffer.
	/// - Throws: If `offset` and `length` defined a region that overlaps
	///           either end of the buffer.
	public func sanitise(offset: Int, length: Int, bufferCount: Int) throws
	{
		guard offset >= 0 else { throw IO.Error.negativeByteOffset }
		guard length >= 0 else { throw IO.Error.negativeByteLength }
		guard offset + length <= bufferCount else { throw IO.Error.byteOverflow }
	}
}
/// A protocol that can be adopted by streams that have the capability to mark
/// a place in their input to return to later.
public protocol Markable
{
	/// Marks the current location in the stream
    ///
	/// Marks the input stream at the current point so that, if `reset()` is
	/// called we can return to this point. If `mark` is unsupported, this may
	/// fail silently.
	/// - Parameter readLimit: Once the stream has read past the `readLimit` it
	///                        is allowed to foget the mark.
	func mark(limit: Int)

	/// Repositions the stream to the point where the mark was set.
	///
	/// - Throws: If `mark(readLimit:)` hasn't been called or if the mark has
	///           been forgotten due to exceeding `readLimit`.
	func reset() throws
}

public struct IO
{
	private init(){}

	/// Errors that can  be thrown by the IO code.
	///
	/// Note that implementations of the protocols need not throw just these
	/// errors. They might choose to pass up OS errors.
	public enum Error: Swift.Error
	{
		/// A call was made to `mark(limit:)` without a mark previously being
		/// set.
		case noMarkSet
		/// Throw if the offset to `read(bytes:, offset:, length:)` is negative
		case negativeByteOffset
		/// Throw if the length to `read(bytes:, offset:, length:)` is negative
		case negativeByteLength
		/// Throw if the `offset + length` to `read(bytes:, offset:, length:)`
		/// is to big to fit in `bytes`
		case  byteOverflow
		/// Thrown if an attempt is made to open a file using a non file URL
		case isNotFileUrl(URL)
		/// Failed to open a file
		///
		/// - Parameters:
		///   - `URL` the file's URL
		///   - `Int32` the posix error code
		case failedToOpen(URL, Int32)
		/// Failed to close a file
		///
		/// - Parameters:
		///   - `URL` the file's URL
		///   - `Int32` the posix error code
		case fileClose(URL, Int32)
		/// An IO operation was attempted on a file that wasn't open
		///
		/// - Parameters:
		///   - `URL` the file's URL
		case fileNotOpen(URL)
		/// FAn IO operation failed on a file
		///
		/// - Parameters:
		///   - `URL` the file's URL
		///   - `Int32` the posix error code
		case io(URL, Int32)
		/// Failed to seek in a file
		///
		/// - Parameters:
		///   - `URL` the file's URL
		///   - `Int32` the posix error code
		case failedSeek(URL, Int32)
	}

	/// Turns an Int32 into its posix error string
	/// - Parameter code: The posix error number
	/// - Returns: A string description of the error number
	public static func posixErrorString(code: Int32) -> String
	{
		guard let rawString = strerror(code) else { return "unknown error" }
		return String(cString: rawString)
	}
}

/// An input stream that reads successive bytes from an array
open class ArrayInStream: InStream, Markable
{
	private let array: [UInt8]
	private var location: Int = 0
	private var markIndex: Int?
	private var markLimit: Int = 0

	public init(_ array: [UInt8])
	{
		self.array = array
	}

	public func available() throws -> Int
	{
		return array.count - location
	}

	public func read() -> UInt8?
	{
		guard location < array.count else { return nil }
		let ret = array[location]
		location += 1
		checkMark()
		return ret
	}

	public func readAllBytes() -> Data
	{
		let ret: Data
		if location == array.count
		{
			ret = Data()
		}
		else
		{
			ret = Data(array[location ..< array.count])
		}
		location = array.count
		checkMark()
		return ret
	}

	public func read(bytes: inout Data, offset: Int, length: Int) throws -> Int
	{
		try sanitise(offset: offset, length: length, bufferCount: bytes.count)
		// If the length is zero, we just return zero, even if we were at enfdo of stream
		guard length != 0 else { return 0 }
		// Make sure we have some bytes to read
		guard location < array.count else { return -1 }

		// Now we can actually read something out of the array
		let copyCount = min(length, array.count - location)

		for i in 0 ..< copyCount
		{
			bytes[offset + i] = array[location + i]
		}
		location += copyCount
		checkMark()
		return copyCount
	}

	public func mark(limit: Int)
	{
		markIndex = location
		markLimit = limit
	}

	public func reset() throws
	{
		guard let markIndex = markIndex
			else { throw IO.Error.noMarkSet }
		location = markIndex
	}

	private func checkMark()
	{
		if let markIndex = markIndex
		{
			if location - markIndex > markLimit
			{
				self.markIndex = nil
			}
		}
	}

	/// The `close` function hass no effect.
	public func close() { }
}


