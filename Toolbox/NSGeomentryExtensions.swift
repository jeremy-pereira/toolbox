//
//  NSGeometryExtensions.swift
//  
//  Created by Jeremy Pereira on 26/01/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension CGSize
{
	/// Multiply the width and height by a constant
	/// - Parameters:
	///   - lhs: The NSSize
	///   - rhs: The constant
	/// - Returns: A new `NSSize` scaled by the constant.
	public static func * (lhs: CGSize, rhs: CGFloat) -> CGSize
	{
		return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
	}

	/// Divide the width and height by a constant
	/// - Parameters:
	///   - lhs: The NSSize
	///   - rhs: The constant
	/// - Returns: A new `NSSize` scaled by the reciprocal of the constant.
	public static func / (lhs: CGSize, rhs: CGFloat) -> CGSize
	{
		return CGSize(width: lhs.width / rhs, height: lhs.height / rhs)
	}

	/// Add two `NSSize`s
	/// - Parameters:
	///   - lhs: An `NSSize`
	///   - rhs: Another `NSSize`
	/// - Returns: The result of adding two sizes: the widths and heights of
	///            each are added.
	public static func + (lhs: CGSize, rhs: CGSize) -> CGSize
	{
		return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
	}
	/// Subrtract one `NSSize` from another.
	/// - Parameters:
	///   - lhs: An `NSSize`
	///   - rhs: Another `NSSize`
	/// - Returns: The result of subtracting `rhs` from `lhs`.
	public static func - (lhs: CGSize, rhs: CGSize) -> CGSize
	{
		return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
	}
}


/// Translate a point by adding an `NSSize` to it
/// - Parameters:
///   - lhs: The point
///   - rhs: The size
/// - Returns: A new point created by adding the size's width to `x` and the
///            size's height to `y`
public func + (lhs: CGPoint, rhs: CGSize) -> CGPoint
{
	return CGPoint(x: lhs.x + rhs.width, y: lhs.y + rhs.height)
}
