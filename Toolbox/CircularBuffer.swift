//
//  CircularBuffer.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 24/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// A sequence based on a collection that never ends and loops back to the
/// beginning of the collection when it reaches the end.
///
/// This wraps an underlying collection which will probably be OK if it
/// mutates while iterating as long as indexes remain valid.
public struct CircularIterator<BackingType: Collection>: Sequence, IteratorProtocol
{
	private let underlyingCollection: BackingType
	private var index: BackingType.Index

	/// Initialise a circular iterator with the given underlying collection.
	///
	/// The itertor will always start with the first element of the underlying
	/// collection.
	///
	/// - Parameter underlyingCollection: The underlying collection to use.
	public init(_ underlyingCollection: BackingType)
	{
		self.underlyingCollection = underlyingCollection
		self.index = self.underlyingCollection.startIndex
	}

	/// Get the next element in the sequence.
	///
	/// Wraps back to the beginning when it hits the end of the underlying
	/// collection. This will only return `nil` if the underlying collection is
	/// empty.
	/// - Returns: The next element in the sequence.
	public mutating func next() -> BackingType.Element?
	{
		guard underlyingCollection.count > 0 else { return nil }
		let ret = underlyingCollection[index]

		index = underlyingCollection.index(index, offsetBy: 1)
		if index == underlyingCollection.endIndex
		{
			index = underlyingCollection.startIndex
		}
		return ret
	}
}
