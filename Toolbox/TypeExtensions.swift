//
//  TypeExtensions.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 28/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import Foundation


prefix operator &++
postfix operator &++
prefix operator &--
infix operator &+=: AssignmentPrecedence
infix operator &-=: AssignmentPrecedence


public extension UInt8
{
    /// The byte as a hex string
	var hexString: String { return String(format: "%02lx", Int(self)) }

	static let asciiTab: UInt8 = 0x09
	static let asciiSpace: UInt8 = 0x20
	static let asciiDot: UInt8 = 0x2e
	static let asciiSlash: UInt8 = 0x2f
	static let ascii0: UInt8 = 0x30
}

public extension UInt16
{
    /// The number as a hex string
	var hexString: String { return String(format: "%04lx", Int(self)) }
    /// The low byte of the int
	var lowByte: UInt8
    {
        get { return UInt8(self & 0xff) }
        set { self = (self & 0xFF00) | UInt16(newValue) }
    }
    /// The high byte of the int
	var highByte: UInt8
    {
        get { return UInt8(self >> 8) }
        set { self = (self & 0xff) | (UInt16(newValue) << 8) }
    }

	init(low: UInt8, high: UInt8)
    {
        self.init(UInt16(high) << 8 | UInt16(low))
    }

	static func &+= (operand: inout UInt16, value: UInt16)
    {
        operand = operand &+ value
    }

	static func &-= (operand: inout UInt16, value: UInt16)
    {
        operand = operand &- value
    }

	static prefix func &--(operand: inout UInt16) -> UInt16
    {
        operand = operand &- 1
        return operand
    }

    /// Postfix increment operator with wrap around on overflow
    ///
    /// - Parameter i: number to be incremented
    /// - Returns: The number as it was before it was incremented
	@discardableResult static postfix func &++(i: inout UInt16) -> UInt16
    {
        defer { i = i &+ 1 }
        return i
    }


    /// Prefix increment operator with wrap around on overflow
    ///
    /// - Parameter i: Number to be incremented
    /// - Returns: The number as it is after the increment
	@discardableResult static prefix func &++(i: inout UInt16) -> UInt16
    {
        i = i &+ 1
        return i
    }

}

public extension String
{
    /// Initialise a string from bytes with the given envoding.
    init?<T: Sequence>
        (uint8Sequence: T, encoding: String.Encoding) throws where T.Iterator.Element == UInt8
    {
        let data = Data(uint8Sequence)
        self.init(data: data, encoding: String.Encoding.utf8)
    }

	func withoutLastComponentDelimitedBy(_ delimiter: String) -> String
    {
        let components = self.components(separatedBy: delimiter)
        let componentSlice = components[0 ..< components.count - 1]
    	return componentSlice.reduce("")
        {
			newString, component in
            return newString + (newString != "" ? delimiter : "") + component
        }
    }

	func bytes(encoding: String.Encoding = String.Encoding.isoLatin1) throws -> [UInt8]
    {
        guard let data = self.data(using: encoding)
            else { throw ToolboxError.invalidStringForEncoding(self, encoding) }
        var bytes: [UInt8] = [UInt8](repeating: 0, count: data.count)
        (data as NSData).getBytes(&bytes, length: bytes.count)
        return bytes
    }


	func signedBytes(encoding: String.Encoding = String.Encoding.isoLatin1) throws -> [Int8]
    {
        guard let data = self.data(using: encoding)
            else { throw ToolboxError.invalidStringForEncoding(self, encoding) }
        var bytes: [Int8] = [Int8](repeating: 0, count: data.count)
        (data as NSData).getBytes(&bytes, length: bytes.count)
        return bytes
    }

}


public extension Sequence where Self.Iterator.Element == UInt8
{
    /// Given a sequence of ASCII digits make an integer from them
    ///
    /// - throws: If the sequence contains any characters not in the range
    ///           `0x30 ..< 0x3a`
    ///
    /// - returns: The integer represented by the ASCII digits
	func makeIntFromAscii() throws -> Int
    {
        if self.first(where: { $0 < 0x30 || $0 > 0x39 }) != nil
        {
            throw ToolboxError.invalidAsciiNumber(Array(self))
        }
        let digitPart = self.map { $0 - UInt8.ascii0 }
        return digitPart.reduce(Int(0)) { $0 * 10 + Int($1) }
    }
}

public extension Sequence
{
	/// Same as normal `reduceInto(into:_:) `except can stop half way through
	///
	/// An extra `inout` parameter is passed to the closure which can be set to
	/// true during any invocation to stop processing at that point. The current
	/// accumulated result is returned to the caller if that is the case.
	/// - Parameters:
	///   - initialResult: The starting result
	///   - updateAccumulatingResult: A closure that is passed the accumulated
	///                                result and a `Bool` as `inout`
	///                                parameters. Change `stop` to true to
	///                                stop further invocations and return at that
	///                                point.
	/// - Returns: A tuple consisting of the final accumulated result and a `Bool`
	///            which is `true` only if all elements in the sequence were
	///            processed.
	func reduceWithStop<Result>(into initialResult: Result,
								_ updateAccumulatingResult: (inout Result, Element, inout Bool) throws -> ()) rethrows -> (Result, Bool)
	{
		var accumulator = initialResult
		var stop = false
		var iterator = self.makeIterator()
		while let element = iterator.next(), !stop
		{
			try updateAccumulatingResult(&accumulator, element, &stop)
		}
		return (accumulator, !stop)
	}
}

@discardableResult public postfix func ++(i: inout Int) -> Int
{
    defer { i += 1 }
    return i
}

@discardableResult public prefix func ++(i: inout Int) -> Int
{
    i += 1
    return i
}

@discardableResult public postfix func ++(i: inout UInt16) -> UInt16
{
    defer { i += 1 }
    return i
}

@discardableResult public prefix func ++(i: inout UInt16) -> UInt16
{
    i += 1
    return i
}

precedencegroup FuncCompositionPrecedence {
	higherThan: BitwiseShiftPrecedence
	associativity: left
}

infix operator >>>: FuncCompositionPrecedence

/// Function composition operator for functions that can throw
///
/// - parameter left:  Left function
/// - parameter right: Right function
///
/// - returns: A function that applies left then right to the result
///
public func >>> <T, U, V> (left: @escaping (T) throws -> U, right: @escaping (U) throws -> V) -> (T) throws -> V
{
    return { t in try right( try left(t)) }
}

/// Function composition operator for functions that cannot throw
///
/// - parameter left:  Left function
/// - parameter right: Right function
///
/// - returns: A function that applies left then right to the result
///
public func >>> <T, U, V> (left: @escaping (T)  -> U, right: @escaping (U)  -> V) -> (T) -> V
{
    return { t in right(left(t)) }
}

/// Turns a `FileHandle` into a `TextOutputStream`
extension FileHandle : @retroactive TextOutputStream
{
	/// Writes the string tp the file handle
	///
	/// Use with care because errors cannot be caught with this method. It's
	/// probably only suitable for logging, which is what we use for internally.
	/// - Parameter string: The string to write. IT will be written explicitly
	///                     as UTF-8.
  	public func write(_ string: String)
	{
    	guard let data = string.data(using: .utf8) else { return }
    	self.write(data)
  	}
}

extension MutableCollection
{
	/// Iterate through all the elements modifying each one.
	/// - Parameter block: A block that modifies its argument which is `inout`
	/// - Throws: if the block throws for any reeason
	public mutating func mutateEach(_ block: (inout Element) throws -> ()) rethrows
	{
		var index = self.startIndex
		while index != self.endIndex
		{
			try block(&self[index])
			index = self.index(after: index)
		}
	}
}

/// Non returning function to flag implementation not existing
///
/// If called, will cause a fatal error
/// - Parameter message: A message to print when executed
/// - Parameter file: The file to report, defaults to file of the call.
/// - Parameter line: The line to report. Defaults to the line of the call.
/// - Parameter function: The name of the function to report, defaults to the calling function.
public func notImplemented(_ message: String = "", file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) not implemented, '\(message)'")
}


/// Marker for functions in super classes that must be overridden
///
/// If called will cause a fatal error
///
/// - Parameter object: the object on which the member function was called. Used
///                     to obtain the dynamic type of the object.
/// - Parameter file: The file to report, defaults to the file in which the
///                   superclass function is.
/// - Parameter line: The line number to be reported. Defaults to the line of the
///                   `mustBeOverridden()`
/// - Parameter function: The name of the function to be overridden, defaults to
///                       the function containing the `mustBeOverridden()`
public func mustBeOverridden(_ object: AnyObject, file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) must be overridden in \(type(of: object))")
}

public extension Dictionary where Value: RangeReplaceableCollection
{
	/// If the dictionary values are arrays, allows us to append a new value
	/// to the array for a given key
	///
	/// If the key is not in the dictionary crerates a new one with an array
	/// containing a single value.
	/// - Parameters:
	///   - value: The value element to add to the value
	///   - key: The key for the new value element
	mutating func append(value: Value.Element, for key: Key)
	{
		if var oldValue = self[key]
		{
			oldValue.append(value)
			self[key] = oldValue
		}
		else
		{
			self[key] = Value([value])
		}
	}
}
