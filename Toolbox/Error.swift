//
//  Error.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 29/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//
import Foundation
///    Errors that can be thrown by toolbox methods.
public enum ToolboxError: Error
{
    case invalidBytesForEncoding([UInt8], String.Encoding)
    case invalidDottedNumber([UInt8])
    case invalidAsciiNumber([UInt8])
    case invalidStringForEncoding(String, String.Encoding)
    case osError(errno: Int32, message: String)
    case stackUnderflow
	case orderedArrayOrder(Int)
	case orderedArrayInvalidHighestKey
}

@available(*, deprecated, message: "Just use // TODO: ...")
public func TODO() -> Bool { return true }
