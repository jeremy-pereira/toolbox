//
//  DottedNumber.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 28/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

/// Represents a dotted number like a version number
public struct DottedNumber: CustomStringConvertible
{
    let parts: [Int]

    public init(_ parts: [Int])
    {
        self.parts = parts
    }

    /// Initialise from an Ascii string encoded as a UINT8 array
    /// - Parameter ascii: Array reprsenting an ascii string like `1.1.0` etc.
    /// - Throws: If the string is not ascii numerals separated by dots.
    public init<T: Sequence>(ascii: T) throws where T.Iterator.Element == UInt8
    {
        let asciiParts = ascii.split(separator: UInt8.asciiDot).map{ Array($0) }

        let parts = try asciiParts.map{ try $0.makeIntFromAscii() }
        self.init(parts)
    }

    public init(string: String) throws
    {
		try self.init(ascii: string.utf8)
    }

    public var description: String
    {
        return parts.map{ "\($0)" }.reduce(""){ $0 + ($0 == "" ? "" : ".") + $1 }
    }

    fileprivate func extendedParts(toCount: Int) -> [Int]
    {
        var ret = self.parts
        while ret.count < toCount
        {
            ret.append(0)
        }
        return ret
    }

}

extension DottedNumber: Equatable
{
    public static func == (a: DottedNumber, b: DottedNumber) -> Bool
    {
        let maxCount = max(a.parts.count, b.parts.count)
        let aParts = a.extendedParts(toCount: maxCount)
        let bParts = b.extendedParts(toCount: maxCount)
        for (aPart, bPart) in zip(aParts, bParts)
        {
            guard aPart == bPart else { return false }
        }
        return true
    }
}

extension DottedNumber: Comparable
{
    public static func < (a: DottedNumber, b: DottedNumber) -> Bool
    {
        let maxCount = max(a.parts.count, b.parts.count)
        let aParts = a.extendedParts(toCount: maxCount)
        let bParts = b.extendedParts(toCount: maxCount)
        for (aPart, bPart) in zip(aParts, bParts)
        {
            guard aPart >= bPart else { return true }
        }
        return false
    }
}
