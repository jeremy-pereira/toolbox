//
//  ShuntingYard.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 03/02/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

private let log = Logger.getLogger("Toolbox.ShuntingYard")

/// Protocol for objects that can be shunted by the sunting yard algorithm
public protocol Shuntable
{
    associatedtype PrecendenceType: Comparable
    /// Operands are numbers and identifiers. They get sent straight to the
    /// output when they are encountered.
    var isOperand: Bool { get }

    /// True if the token is an operator
    var isOperator: Bool { get }

    /// True if the token is left parenthesis
    var isLPar: Bool { get }
    /// True if the token is right parenthesis
    var isRPar: Bool { get }

    /// The precedence of this token
    var precedence: PrecendenceType { get }

    /// The associativity of operators.
    var isLeftAssociative: Bool { get }

}

/// A type that is used for parsing expressions using the shunting yard
/// algorithm.
public struct ShuntingYard<Token: Shuntable>
{
	public init()
    {

    }
    /// Process an infix expression to produce a reverse polish expression
    ///
    /// - Parameter input: The tokenised infix expression
    /// - Returns: The same tokens organised as a reverse Polish expression
    /// - Throws: If the expression is not well formed
    public func shunt(expression input: [Token]) throws -> [Token]
    {
        var operatorStack = Stack<Token>()
        var output: [Token] = []

        for token in input
        {
            if token.isOperand
            {
                output.append(token)
            }
            else if token.isOperator
            {
                while let stackOperator = operatorStack.top,
                    (stackOperator.precedence > token.precedence
                     || stackOperator.precedence == token.precedence && stackOperator.isLeftAssociative)
                    && !stackOperator.isLPar
                {
                    output.append(operatorStack.pop()!)
                }
                operatorStack.push(token)
            }
            else if token.isLPar
            {
				operatorStack.push(token)
            }
            else if token.isRPar
            {
                var lParFound = false
				while let topToken = operatorStack.pop()
                {
                    guard !topToken.isLPar
                    else
                    {
                        lParFound = true
                        break
                    }
                    output.append(topToken)
                }
                guard lParFound else { throw Error.unmatchedRightParenthesis }
            }
            log.debug("Token: \(token), stack: \(operatorStack), output: \(output)")
        }
        while !operatorStack.isEmpty
        {
            guard let token = operatorStack.pop(), !token.isLPar
                else { throw Error.unmatchedLeftParenthesis }
            output.append(token)
        }
        return output
    }
}

extension ShuntingYard
{
    public enum Error: Swift.Error
    {
        case unmatchedRightParenthesis
        case unmatchedLeftParenthesis
    }
}

