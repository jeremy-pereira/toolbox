//
//  Monad.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 28/08/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

// Precedence group - it's a form of function binding so really high precedence
precedencegroup MonadPrecedence {
    higherThan: BitwiseShiftPrecedence
    associativity: left
}

infix operator >>-: MonadPrecedence	// Monadic bind

extension Optional
{
    /// Monad unit function for `Optional`.
	///
	/// Just creates an optional from from the value.
    ///
    /// - Parameter x: The value to be wrapped as an optional
    /// - Returns: `Optional.some(x)`
    public static func unit(_ x: Wrapped) -> Wrapped?
    {
		return Optional(x)
    }


    /// The monad bind for `Optional`
    ///
    /// - Parameter f: A function that has `Self.Wrapped` as the input and
	///                returns another `Optional`
    /// - Returns: An `Optional`.
    public func bind<T>(_ f: (Wrapped) -> Optional<T>) -> Optional<T>
    {
        return flatMap(f)
    }
	/// Monadic bind operator for `Optional`
	///
	/// - Parameters:
	///   - m: A monad, in this case `Optional`, that wraps a value of type `Wrapped`
	///   - f: A function that converts a value of type `Wrapped` into an `Optional` wrapping
	///        an arbitrary type
	/// - Returns: If `m` is `nil` then `nil` else the result of applying `f` to the
	///            value that `m` wraps.
	public static func >>- <U>(_ m: Optional<Wrapped>, _ f: (Wrapped) -> Optional<U>) -> Optional<U>
	{
		return m.flatMap(f)
	}
}


extension Result
{
	/// Monad unit function for `Result`.
	///
	/// Just creates a `Result` from from the value.
	///
	/// - Parameter x: The value to be wrapped as a `Result`
	/// - Returns: `Result.success(x)`
	public static func unit(_ x: Success) -> Result
	{
		return Result.success(x)
	}


	/// Monadic binf for `Result`
	///
	/// - Parameter function: The function to bind this to
	/// - Returns: The result of applying `function` to the value of `self` or
	///            if `self` is `.failure` a failure with the same associated fail
	///            reason
	public func bind<T>(_ function: (Success) -> Result<T, Failure>) -> Result<T, Failure>
	{
		return flatMap(function)
	}
	/// Monadic bind operator for `Result`
	///
	/// - Parameters:
	///   - m: A monad, in this case `Result`, that wraps a value of type `Success` and failure
	///        type `Failure`
	///   - f: A function that converts a value of type `Success` into an `Result` wrapping
	///        an arbitrary type
	/// - Returns: If `m` is a failure then the failure else the result of applying `f` to the
	///            value that `m` wraps.
	public static func >>- <U>(_ m: Result<Success, Failure>, _ f: (Success) -> Result<U, Failure>) -> Result<U, Failure>
	{
		return m.flatMap(f)
	}
}

