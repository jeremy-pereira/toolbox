//
//  LRUCache.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 11/06/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//
fileprivate class Queue<T>
{
    var count: Int = 0
    class Entry
    {
        // The pointers towards the front of the queue are weak to avoid
        // ownership cycles.
        weak var prev: Entry?
        var next: Entry?
        let value: T
        unowned let owner: Queue<T>

        init(owner: Queue<T>, value: T)
        {
            self.value = value
            self.owner = owner
        }
    }
    var first: Entry?
    weak var last: Entry?

    func removeAll()
    {
        first = nil
        last = nil
        count = 0
    }

    func remove(entry: Entry)
    {
        guard entry.owner === self else { fatalError("Attempt to remove queue entry from the wrong queue") }

        if let prev = entry.prev
        {
            prev.next = entry.next
        }
        else
        {
            first = entry.next
        }
        if let next = entry.next
        {
            next.prev = entry.prev
        }
        else
        {
            last = entry.prev
        }
        entry.prev = nil
        entry.next = nil
        count -= 1
    }

    func addFirst(_ key: T) -> Entry
    {
        let entry = Entry(owner: self, value: key)
        if let first = first
        {
            first.prev = entry
            entry.next = self.first
            self.first = entry
        }
        else
        {
            assert(last == nil)
            first = entry
            last = entry
        }
        count += 1
        return entry
    }

    func removeLast() -> Entry?
    {
        guard let ret = last else { return nil }
        assert (ret.next == nil)

        if let prev = ret.prev
        {
            prev.next = nil
            last = prev
        }
        else
        {
            last = nil
            first = nil
        }
        count -= 1
        ret.prev = nil
        return ret
    }
}


/// A least recently used cache. Objects in the cache are retrieved by keys.
/// If an object is not in the cache, the suppliced cacheMiss function is used
/// to retrieve it instead.
///
/// The cache currently retains a strongf reference to its values so even if you
/// get rid of all other references, they will hang around until purged from the
/// cache.
///
/// Note that, internally, the cache is implemented as a Swift dictionary and
/// the order of recent usage is a doubly linked list. There's no point in using
/// this to front anything unless the cacheMiss function is significantly more
/// expensive than accessing the elements of a dictionary. e.g. if it has to
/// retrieve items from disk.
public class LRUCache<Key: Hashable, Value>
{
    let limit: Int
    let cacheMiss: (Key) throws -> Value?


    /// Initialise the cache
    ///
    /// - Parameters:
    ///   - limit: The limit to the number of cache entries, must be more than 0
    ///   - cacheMiss: Function for getting values that are not in the cache.
    ///                The fucntion is allowed to throw and may return nil if it
    ///                cannot find a suitable value for the key.
    public init(limit: Int, cacheMiss: @escaping (Key) throws -> Value?)
    {
        guard limit > 0 else { fatalError("Cache limit must be positive") }

        self.limit = limit
        self.cacheMiss = cacheMiss
    }

    private var lruQueue: Queue = Queue<Key>()

    private class CacheEntry
    {
        fileprivate let value: Value
        fileprivate var lruEntry: Queue<Key>.Entry

        init(value: Value, lruEntry: Queue<Key>.Entry)
        {
            self.value = value
            self.lruEntry = lruEntry
        }
    }

    private var map: [Key : CacheEntry] = [:]

    /// Removes everything from the cache.
    public func invalidate()
    {
        lruQueue.removeAll()
        map.removeAll()
    }

    /// The number of lookups that have been performed by this cache.
    public private(set) var lookups = 0
    /// The number of lookups that required `cacheMiss` to be called.
    public private(set) var misses = 0

    /// Lookup an item in the cache. If the item is not there, the `cacheMiss`
    /// function will be used as a fallback.
    ///
    /// - Parameter key: Key of item to look up.
    /// - Returns: The value for the key or nil if it can't be found.
    /// - Throws: If the `cacheMiss` function throws.
    public func lookup(_ key: Key) throws -> Value?
    {
        lookups += 1
        let ret: Value?
        if let cacheEntry = map[key]
        {
            // Got a hit. Retrieve the value and move the cache entry to the
            // front of the queue.
            ret = cacheEntry.value
            lruQueue.remove(entry: cacheEntry.lruEntry)
            cacheEntry.lruEntry = lruQueue.addFirst(key)
        }
        else
        {
            misses += 1
            if let newValue = try cacheMiss(key)
            {
                // Add a new cache entry for the new value
                ret = newValue
                let cacheEntry = LRUCache.CacheEntry(value: newValue,
                                                     lruEntry: lruQueue.addFirst(key))
                map[key] = cacheEntry
                while lruQueue.count > limit
                {
                    if let lruEntry = lruQueue.removeLast()
                    {
                        map[lruEntry.value] = nil
                    }
                }
            }
            else
            {
                ret = nil
            }
        }
        return ret
    }
}
