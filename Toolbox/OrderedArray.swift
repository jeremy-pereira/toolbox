//
//  OrderedArray.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 29/04/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//

private var log = Logger.getLogger("Toolbox.OrderedArray")


///    An array in which the elements are ordered by some arbitrary key.
///    
///    The user must supply a function to extract an element's key from an element
///    when the array is initialised.
///    The array uses a binary search `O(log n)` to index elements by key.
///    Elements are indexed by Int like a normal array, but the subscript is 
///    read only to prevent elements from becoming unordered.
public struct OrderedArray<Element, Key: Comparable>
{
    fileprivate var elements: [Element] = []
    fileprivate let keyFunction: (Element) -> Key

	fileprivate var highestKey: Key?
	{
		guard let last = elements.last else { return nil }
		return keyFunction(last)
	}

    public init(_ initialElements: [Element], keyFunction: @escaping (Element) -> Key)
    {
        self.keyFunction = keyFunction
        for element in  initialElements
        {
            self.insert(element)
        }
    }

    /// Number of items in the array
    public var count: Int { return elements.count }

    ///    Insert an element at the correct location in the array. Elements 
    ///    with a key that matches an existing element are inserted after the 
    ///    existing element. The function is optimised so that inserting an item
    ///    that should go at the end becomes a simple append.
    ///
    /// - parameter element: The element to insert
    ///
    public mutating func insert(_ element: Element)
    {
        _ = self.insert(element, allowDuplicates: true)
    }


    ///  Insert an element at the correct location in the array. Elements
    ///  with a key that matches an existing element are inserted after the
    ///  existing element unless `allowDuplicates` is false in which case,
    ///  they are not inserted at all.
    ///
    ///  The function is optimised so that inserting an item
    ///  that should go at the end becomes a simple append.
    ///
    /// - Parameters:
    ///   - element: The element to insert
    ///   - allowDuplicates: true if you are allowed to insert duplicates.
    ///
    /// - Returns: true if the element was inserted, false if `allowDuplicates`
    ///            is false and the element is already in the array.
	@discardableResult
    public mutating func insert(_ element: Element, allowDuplicates: Bool) -> Bool
    {
        let elementKey = keyFunction(element)
        guard let highestKey = highestKey, elementKey < highestKey
        else
        {
            elements.append(element)
            return true
        }
        let (insertionPoint, match) = findLowestIndexOf(elementKey)
        if match && allowDuplicates
        {
            if insertionPoint == elements.count - 1
            {
                elements.append(element)
            }
            else
            {
                elements.insert(element, at: insertionPoint + 1)
            }
        }
        else if !match
        {
            elements.insert(element, at: insertionPoint)
        }
        return !match || allowDuplicates
    }

	/// Replace an element by a new one with the same key
	///
	/// If no element with the element's key are present, this is the same as an
	/// insertion.
	/// - Parameters:
	///   - element: The new element
	///   - allowDuplicates: If true and there are multiple elements with the
	///                      same key, only the first one will be replaced. If
	///                      false, the first will be replaced and the others
	///                      will be deleted.
	/// - Returns: the elements that were replaced if any.
	@discardableResult
    public mutating func replace(_ element: Element, allowDuplicates: Bool) -> [Element]
	{
		let ret: [Element]

		let elementKey = keyFunction(element)
		let (existingIndex, exists) = findLowestIndexOf(elementKey)
		if !exists
		{
			elements.insert(element, at: existingIndex)
			ret = []
		}
		else
		{
			var firstToKeep = existingIndex + 1
			if !allowDuplicates
			{
				while firstToKeep < elements.count && keyFunction(elements[firstToKeep]) == elementKey
				{
					firstToKeep += 1
				}
			}
			let replaceRange = existingIndex ..< firstToKeep
			ret = Array(elements[replaceRange])
			elements.removeSubrange(replaceRange)
		}
		return ret
	}

    /// Get the slice of the ordered array between the interval of keys.
    ///
    /// - parameter interval: An intervale between two keys
    ///
    /// - returns: An array slice of the elements whose keys are in the interval
    public subscript(interval: Range<Key>) -> ArraySlice<Element>
    {
        return self[from: interval.lowerBound, to: interval.upperBound]
    }

    public subscript(from from: Key, to to: Key) -> ArraySlice<Element>
    {
        let (fromIndex, _) = findLowestIndexOf(from)
        let (toIndex, _) = findLowestIndexOf(to)
        return elements[fromIndex ..< toIndex]
    }


    /// Finds the first item with the given key or the insertion point if it is
    /// not there.
    /// - Parameter key: Key to look for
    /// - Returns: A tuple consisting of the index of the item/insertion point 
    ///            and a bool which is true if the item exists. or false if
    ///            the index represents the insertion point.
    public func findLowestIndexOf(_ key: Key) -> (Int, Bool)
    {
        var min = elements.startIndex
        var max = elements.endIndex
        var foundIndex: Int?

        log.debug("Looking for key \(key)")

        while foundIndex == nil && max > min
        {
            let candidate = min + (max - min) / 2
            log.trace("min = \(min), max = \(max), candidate = \(candidate)")
            let candidateKey = keyFunction(elements[candidate])
            if candidateKey == key
            {
                log.trace("Exact match \(candidateKey)")
                if candidate == elements.startIndex
                	|| keyFunction(elements[candidate - 1]) < key
                {
                    foundIndex = candidate
                }
                else
                {
                    log.trace("but match is not the first match")
                    max = candidate
                }
            }
            else if candidateKey < key
            {
                log.trace("Candidate \(candidateKey) too small")
               	min = candidate + 1
            }
            else // candidateKey > key
            {
                log.trace("Candidate \(candidateKey) too big")
                max = candidate
            }
        }
        let ret: (Int, Bool)
        if let foundIndex = foundIndex
        {
            ret = (foundIndex, true)
        }
        else
        {
            ret = (max, false)
        }
        log.debug("Found = (\(ret.0), \(ret.1))")
        return ret
    }


	/// Finds the first element with the given key
	/// - Parameter key: key to look for
	/// - Returns: the first element with the given key
	public func first(withKey key: Key) -> Element?
	{
		let (index, found) = findLowestIndexOf(key)
		return found ? self[index] : nil
	}

	/// The index of the first element with the given key
	/// - Parameter key: The key to look for
	/// - Returns: The index for the key or `nil` if the key is not there
	public func index(ofKey key: Key) -> Int?
	{
		let (index, found) = findLowestIndexOf(key)
		return found ? index : nil
	}

	/// Removes the element at the given index
	/// - Parameter index: The index of the element to be removed
	/// - Returns: the element that was removed.
	@discardableResult
	public mutating func remove(at index: Int) -> Element
	{
		return elements.remove(at: index)
	}

	/// Remove all the elements with the given key
	/// - Parameter key: The key identifying the elements to be removed
	/// - Returns: An array ofd the elements removed
	@discardableResult
	public mutating func removeAll(key: Key) -> [Element]
	{
		let ret = elements.filter { keyFunction($0) == key }
		elements.removeAll { keyFunction($0) == key }
		return ret
	}
}

extension OrderedArray
{

	/// Checks the internal consistency of the array
	///
	/// This makes sure all the elements are still ordered and, if they are not
	/// throws an error
	///
	/// This function is used to check the integrity of the array in unit tests.
	///
	/// - Returns: if a key is out of order, returns `ToolboxError.orderedArrayOrder`
	///            with a parameter that is the index of the out of order element.
	///            If the highestKey is not actually the highest key, returns
	///           `ToolboxError.orderedArrayInvalidHighestKey`
	func checkConsistency() -> ToolboxError?
	{
		guard count > 1 else { return nil }
		for index in 1 ..< self.count
		{
			guard keyFunction(self[index]) >= keyFunction(self[index - 1])
				else { return ToolboxError.orderedArrayOrder(index) }
		}
		if let last = elements.last,
		   let highestKey = self.highestKey,
		   keyFunction(last) != highestKey
		{
			return ToolboxError.orderedArrayInvalidHighestKey
		}
		else if (self.highestKey != nil && elements.last == nil)
			 || (self.highestKey == nil && elements.last != nil)
		{
			return ToolboxError.orderedArrayInvalidHighestKey
		}
		return nil
	}
}

extension OrderedArray: RandomAccessCollection
{
    public var startIndex: Int { return 0 }

    public var endIndex: Int { return self.count }

    public func index(after: Int) ->Int
    {
		return after + 1
    }

    public subscript(index: Int) -> Element
    {
        return elements[index]
    }
}

extension OrderedArray: ExpressibleByArrayLiteral
	where Element: Comparable, Key == Element
{
	public init(arrayLiteral elements: Element...)
	{
		self.init(elements, keyFunction: { $0 })
	}
}

/// A dictionary organised as an array ordered by key
///
/// This differs from a normal dictionary where the keys are hashed in that
/// the keys are organised in ascending order and found by doing a binary
/// search. This means that access time is `O(log n)`. Insertion time is
/// probsably `O(n)` because the underlying store is an `OrderedArray`.
public struct BinarySearchMap<Key: Comparable, Value>
{
	private var elements: OrderedArray<(Key, Value), Key>

	public init()
	{
		elements = OrderedArray([]){ $0.0 }
	}

	public subscript(position: Key) -> Value?
	{
		get
		{
			guard let (_, value) = elements.first(withKey: position) else { return nil }
			return value
		}
		set
		{
			if let value = newValue
			{
				elements.replace((position, value), allowDuplicates: false)
			}
			else
			{
				elements.removeAll(key: position)
			}
		}
	}
	/// The keys of this map
	///
	/// The keys will be ordered in ascending order
	public var keys: [Key]
	{
		return elements.map { $0.0 }
	}
	/// The values of this map
	///
	/// The values will be ordered by ascending order of their keys
	public var values: [Value]
	{
		return elements.map { $0.1 }
	}
}

extension BinarySearchMap: ExpressibleByDictionaryLiteral where Key: Comparable
{
	public init(dictionaryLiteral elements: (Key, Value)...)
	{
		self.init()
		for (key, value) in elements
		{
			self[key] = value
		}
	}
}

extension BinarySearchMap: Collection
{
	public func index(after i: Int) -> Int { i + 1 }

	public subscript(position: Int) -> (Key, Value)
	{
		return elements[position]
	}

	public var startIndex: Int { elements.startIndex }

	public var endIndex: Int { elements.endIndex }
}
