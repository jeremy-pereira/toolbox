//
//  PRNG.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 12/09/2016.
//  Copyright © 2016, 2019 Jeremy Pereira. All rights reserved.
//

import Foundation



public extension RandomNumberGenerator
{
	/// Generates two random numbers with a normal distribution.
	///
	/// This method uses the [Box-Muller transform](https://en.wikipedia.org/wiki/Box–Muller_transform)
	///
	/// - Parameters:
	///   - mean: The required mean
	///   - sd: The required standard deviation
	/// - Returns: Two `Double`s each of which will be distributed normally
	///            around the mean and with the given standard deviation
	mutating func generateGaussianNoise(mean: Double, sd: Double) -> (Double, Double)
	{
		// u1 cannot be zero because, if it were, we'd be doing log(0) later.
		// I think both number should theoretically be in the range (0, 1)
		let u1 = 1 - Double.random(in: 0 ..< 1, using: &self)
		let u2 = Double.random(in: 0 ... 1, using: &self)

		let z0 = sqrt(-2.0 * log(u1)) * cos(2 * Double.pi * u2)
		let z1 = sqrt(-2.0 * log(u1)) * sin(2 * Double.pi * u2)
		return (z0 * sd + mean, z1 * sd + mean)
	}

	/// A yes no selection based on a probability.
	///
	/// This is like flipping a coin except the probability of "heads" can be
	/// varied between 0 and 1
	///
	/// - Parameter probability: The probability of returning `true`
	/// - Returns: true or false with random probability
	mutating func select(_ probability: Double) -> Bool
	{
		assert(probability >= 0 && probability <= 1)
		guard probability > 0 else { return false }
		guard probability < 1 else { return true }

		let selection = Double.random(in: 0 ... 1, using: &self)
		return selection < probability
	}

}

/// A wrapper for a random number generator.
///
/// We need this because `Double.random(using:)` can't accept a `RandomNumberGenerator`
/// since it doesn't seem to conform to itself. Additionally, making it a class
/// allows us to pass it without it being an `inout` argumengt.
public class AnyRNG: RandomNumberGenerator
{
	private var wrapped: RandomNumberGenerator
	private init<T: RandomNumberGenerator>(wrapped: T)
	{
		self.wrapped = wrapped
	}


	/// Initialises an `AnyRNG` that wraps the system random number generator.
	public convenience init()
	{
		self.init(wrapped: SystemRandomNumberGenerator())
	}


	/// Wrap a random numbewr generator as an `AnyRNG`
	///
	/// If the RNG is already an `AnyRNG` just return it.
	///
	/// - Parameter wrapped: The RNG to wrap
	/// - Returns: An `AnyRNG` that wraps the original RNG
	public static func wrap<T: RandomNumberGenerator>(_ wrapped: T) -> AnyRNG
	{
		if let anyRNG = wrapped as? AnyRNG
		{
			return anyRNG
		}
		else
		{
			return AnyRNG(wrapped: wrapped)
		}
	}

	public func next() -> UInt64
	{
		return wrapped.next()
	}

	/// Return an `Int` in the range `0 ..< upperBound`
	///
	/// - Parameter upperBound: Must be > 0
	/// - Returns: A random integer
	public func zeroBasedRandom(_ upperBound: Int) -> Int
	{
		guard upperBound > 0 else { fatalError("Upper bound must be greater than 0") }
		return Int(wrapped.next(upperBound: UInt(upperBound)))
	}
}
