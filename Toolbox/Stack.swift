//
//  Stack.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 20/11/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

///    A simple stack
public struct Stack<Element>
{
	/// All the elements on the stack
    public private(set) var elements: [Element] = []

    public init() {}

    ///    Push an element onto the top of the stack
    ///
    /// - parameter newElement: The element to push
    public mutating func push(_ newElement: Element)
    {
		elements.append(newElement)
    }

    ///    Pops the top element off the stack
    ///
    ///    - returns: The top element or nil if the stack is empty.
    public mutating func pop() -> Element?
    {
        guard let ret = elements.last else { return nil }
        elements.removeLast()
        return ret
    }

    /// Pop the top element off the stack.
    ///
    /// If the stack is empty an error is thrown
    ///
    /// - Returns: The top element of the stack
    /// - Throws: If the stack is empty
    public mutating func throwingPop() throws -> Element
    {
        guard let ret = self.pop() else { throw ToolboxError.stackUnderflow }
        return ret
    }

    ///  The top element of the stack. Will be nil if the stack is empty
    public var top: Element?
    {
        return elements.last
    }

    /// Number of items in the stack
    public var count: Int
    {
		return elements.count
    }

    /// True if the stack is empty
    public var isEmpty: Bool
    {
		return elements.isEmpty
    }
}
