//
//  OrderedArrayTests.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 29/04/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class OrderedArrayTests: XCTestCase
{

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testInit()
    {
        let testValues = [2, 3, 1, 8, 3]
        let myArray = OrderedArray(testValues, keyFunction: { $0 })
        XCTAssert(myArray.count == testValues.count, "Wrong count")
        var previousElement = -1
        for element in myArray
        {
            XCTAssert(element >= previousElement, "Array is unordered")
            previousElement = element
        }

		if let failedIndex = myArray.checkConsistency()
		{
			XCTFail("Index \(failedIndex) is out of order")
		}

    }

    struct TestStruct
    {
        let value: Int
        let unneeded: String
    }

    func testRetrieve()
    {
        let testValues = [2, 3, 1, 8, 3].map{ TestStruct(value: $0, unneeded: "") }
        let myArray = OrderedArray(testValues, keyFunction: { $0.value })
        XCTAssert(myArray.count == testValues.count, "Wrong count")

        let retrievedValues = myArray[from: 3, to: 8]
        if retrievedValues.count != 2
        {
            XCTFail("Didn't get the right set of values: \(retrievedValues)")
            return
        }
        XCTAssert(retrievedValues[retrievedValues.startIndex].value == 3, "Got wrong value")
        let emptyValues = myArray[from: 34, to: 25]
    	if emptyValues.count == 0
        {
            return
        }
        XCTFail("Should not get value")
    }

    func testRetrieveNonUnique()
    {
        Logger.pushLevel(.trace, forName: "Toolbox.OrderedArray")
        // The array below needs to be set up to get the second 2 to test the 
        // mechanism for finding the first of a non unique value.
        let testValues = [-2, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3].map{ TestStruct(value: $0, unneeded: "") }
        let myArray = OrderedArray(testValues, keyFunction: { $0.value })
        XCTAssert(myArray.count == testValues.count, "Wrong count")

        let retrievedValues = myArray[from: 2, to: 3]
        if retrievedValues.count != 4
        {
            XCTFail("Didn't get the right set of values: \(retrievedValues)")
            return
        }
        XCTAssert(retrievedValues[retrievedValues.startIndex].value == 2, "Got wrong value")
        Logger.popLevel(forName: "Toolbox.OrderedArray")

    }

    func testInsertNoDupes()
    {
    	let testValues = [1, 2, 5, 6]
    	var theArray = OrderedArray(testValues) { $0 }
    	XCTAssert(!theArray.insert(1, allowDuplicates: false), "Allowed to insert an existing element")
        XCTAssert(theArray.insert(1, allowDuplicates: true), "Not allowed to insert an existing element")
        XCTAssert(theArray.count == 5, "Failed to insert into \(theArray)")
        XCTAssert(theArray.insert(3, allowDuplicates: false), "Not llowed to insert an non existing element")
        XCTAssert(theArray.insert(4, allowDuplicates: true), "Not allowed to insert an enon xisting element")
        XCTAssert(theArray.count == 7, "Failed to insert into \(theArray)")

		if let failedIndex = theArray.checkConsistency()
		{
			XCTFail("Index \(failedIndex) is out of order")
		}
    }

	func testArrayLiteral()
	{
		let testValues: OrderedArray<Int, Int> = [2, 1, 4, 3]

		testValues.enumerated().forEach
		{
			pair in
			XCTAssert(pair.0 == pair.1 - 1, "Invalid position \(pair)")
		}
	}

	func testfirstAndIndexWithKey()
	{
		let testValues = OrderedArray([-2, 1, 2, 3]) { $0 * $0 }

		XCTAssert(testValues.index(ofKey: 4) == 1)
		XCTAssert(testValues.first(withKey: 4) == -2)
		if let failedIndex = testValues.checkConsistency()
		{
			XCTFail("Index \(failedIndex) is out of order")
		}
	}

	func testRemoveAll()
	{
		var testValues: OrderedArray<Int, Int> = [2, 1, 1, 4, 1, 3]

		XCTAssert(testValues.count == 6)
		testValues.removeAll(key: 1)
		XCTAssert(testValues.count == 3)
	}

	func testBinaryMapAccess()
	{
		var binaryMap: BinarySearchMap<Int, String> = [
			2 : "two",
			1 : "one",
			3 : "three",
			5 : "five"
		]
		XCTAssert(binaryMap[2] == "two")
		XCTAssert(binaryMap[4] == nil)
		XCTAssert(binaryMap.keys == [1, 2, 3, 5])
		XCTAssert(binaryMap.values == ["one", "two", "three", "five"])
		binaryMap[4] = "four"
		XCTAssert(binaryMap.values == ["one", "two", "three", "four", "five"])
		XCTAssert(binaryMap[4] == "four")
		XCTAssert(binaryMap.startIndex == 0)
		XCTAssert(binaryMap.endIndex == 5)
		let two: (Int, String) = binaryMap[1]
		XCTAssert(two.0 == 2)
		XCTAssert(two.1 == "two")
	}

}
