//
//  RandomTests.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 10/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox

class RandomTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testArc4RandomBias()
    {
        var heads = 0
        var tails = 0
        let iterations = 1000
        let error = iterations / 10

        var rng = SystemRandomNumberGenerator()
        for _ in 0 ..< iterations
        {
            if rng.select(0.5)
            {
                heads += 1
            }
            else
            {
                tails += 1
            }
        }
        let diff = heads > tails ? heads - tails : tails - heads
        XCTAssert(diff < error, "Deviation too large: h=\(heads), t=\(tails)")
    }
}
