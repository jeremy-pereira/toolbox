//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 21/03/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox

private let log = Logger.getLogger("ToolboxTests.IOTests")

class IOTests: XCTestCase
{

    override func setUp()
	{
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

	func testArrayStreamRead()
	{
		let arrayStream = ArrayInStream([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
		XCTAssert(try arrayStream.available() == 10)
		XCTAssert(arrayStream.read() == 0)
		XCTAssert(try arrayStream.available() == 9)
		let data = arrayStream.readAllBytes()
		XCTAssert(data.count == 9)
		XCTAssert(data[1] == 2)
		arrayStream.closeQuietly()
	}

	func testArrayStreamReadABit()
	{
		let arrayStream = ArrayInStream([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
		let arrayStream2 = ArrayInStream([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
		var data = Data(count: 6)
		do
		{
			let count = try arrayStream.read(bytes: &data)
			XCTAssert(count == 6)
			XCTAssert(data[0] == 0)
			XCTAssert(data[5] == 5)
			let count2 = try arrayStream.read(bytes: &data)
			XCTAssert(count2 == 4)
			XCTAssert(data[0] == 6)
			XCTAssert(data[3] == 9)
			XCTAssert(data[5] == 5)
			let count3 = try arrayStream.read(bytes: &data)
			XCTAssert(count3 == -1)
			let count4 = try arrayStream2.read(bytes: &data, offset: 2, length: 2)
			XCTAssert(count4 == 2)
			XCTAssert(data[2] == 0)
			XCTAssert(data[3] == 1)
			XCTAssert(data[4] == 4)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testMark()
	{
		let arrayStream = ArrayInStream([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
		_ = arrayStream.read()
		arrayStream.mark(limit: 2)
		guard let byte = arrayStream.read()
			else { fatalError("Failed to read second byte of the array") }
		do
		{
			try arrayStream.reset()
			if let byte2 = arrayStream.read()
			{
				XCTAssert(byte2 == byte)
			}
			else
			{
				XCTFail("No byte read")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testFileInStreamReadSingle()
	{
		log.pushLevel(.info)
		defer { log.popLevel() }
		let (url, _) = makeFile("testFile")
		do
		{
			let stream = try FileInStream(url: url)
			defer { stream.closeQuietly(failAction: { XCTFail("\($0)") } ) }

			while let char = try stream.read(), char != " ".utf8.first!
			{
				XCTAssert(char == "0".utf8.first!)
			}
			if let char = try stream.read()
			{
				XCTAssert(char == "X".utf8.first!)
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testFileInStreamReadAll()
	{
		log.pushLevel(.info)
		defer { log.popLevel() }
		let (url, fileSize) = makeFile("testFile")
		do
		{
			let stream = try FileInStream(url: url)
			defer { stream.closeQuietly(failAction: { XCTFail("\($0)") } ) }
			let data = try stream.readAllBytes()
			XCTAssert(data.count == fileSize)
			guard let readString = String(bytes: data, encoding: .utf8)
			else
			{
				XCTFail("Could not convert data to string")
				return
			}
			let fileString = try String(contentsOf: url, encoding: .utf8)
			XCTAssert(readString == fileString)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testFileInStreamAvailable()
	{
		log.pushLevel(.info)
		defer { log.popLevel() }
		let (url, fileSize) = makeFile("testFile")
		do
		{
			let stream = try FileInStream(url: url)
			defer { stream.closeQuietly(failAction: { XCTFail("\($0)") } ) }

			XCTAssert(try stream.available() == fileSize)
			var data = Data(count: 1000)
			let bytesRead = try stream.read(bytes: &data)
			XCTAssert(bytesRead == data.count)
			XCTAssert(try stream.available() == fileSize - bytesRead)
		}
		catch
		{
			XCTFail("\(error)")
		}

	}

	func makeFile(_ name: String) -> (URL, Int)
	{
		let lineLength = 32
		let requestedSize = 5000
		let actualSize = requestedSize / lineLength * lineLength
		let numberSize = 4
		var string = ""
		// Padding to make each line up to 32 characters including a line number
		// and new line and a space after the line number.
		let padding = String(repeating: "X", count: lineLength - numberSize - 2)
		for i in 0 ..< (actualSize / lineLength)
		{
			let line = String(format: "%0*x ", numberSize, i) + padding + "\n"
			string.append(line)
		}
		precondition(string.count == actualSize, "string size is \(string.count), expected \(actualSize)")
		let fileUrl = URL(fileURLWithPath: name).absoluteURL
		do
		{
			try string.write(to: fileUrl, atomically: false, encoding: .utf8)
			log.info("Created file \(fileUrl) of size \(actualSize)")
		}
		catch
		{
			fatalError("\(error)")
		}
		return (fileUrl, actualSize)
	}

	public static var allTests = 
    [
        ("testArrayStreamRead", testArrayStreamRead),
    ]
}

