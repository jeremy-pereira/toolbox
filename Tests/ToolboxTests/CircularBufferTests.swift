//
//  CircularBufferTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 24/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox

class CircularBufferTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEmptySequence()
	{
		var sequence = CircularIterator([])

		XCTAssert(sequence.next() == nil, "Expected empty sequence")
    }

	func testNonEmptySequence()
	{
		let sequence = CircularIterator([1, 2])

		for (index, (actual, expected)) in zip(sequence, [1, 2, 1, 2]).enumerated()
		{
			XCTAssert(actual == expected, "Got something wrong here at index \(index)")
		}
	}
}
