//
//  DateTests.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 15/10/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class DateTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testHTTPString()
    {
        let aDate = Toolbox.dateFrom(unixTime: 5 * 24 * 3600 + 3600)	// 6th Jan 1970 01:00 GMT
        XCTAssert(aDate.httpString == "Tue, 06 Jan 1970 01:00:00 GMT", "Incorrect date \(aDate.httpString)")
        let date2 = Toolbox.dateFrom(unixTime: 9 * 30 * 5 * 24 * 3600 + 3600 * 18)	// September
        XCTAssert(date2.httpString == "Wed, 12 Sep 1973 18:00:00 GMT", "Incorrect date '\(date2.httpString)'")
    }

    func testISOString()
    {
        let aDate = Toolbox.dateFrom(unixTime: 5 * 24 * 3600 + 3600)	// 6th Jan 1970 01:00 GMT
        XCTAssert(aDate.isoString == "1970-01-06T01:00:00Z", "Incorrect date \(aDate.isoString)")
        let date2 = Toolbox.dateFrom(unixTime: 9 * 30 * 5 * 24 * 3600 + 3600 * 18)	// September
        XCTAssert(date2.isoString == "1973-09-12T18:00:00Z", "Incorrect date \(date2.isoString)")
    }

}
