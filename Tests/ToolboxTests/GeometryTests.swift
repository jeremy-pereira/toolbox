//
//  NSGeometryTests.swift
//  
//
//  Created by Jeremy Pereira on 26/01/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Foundation
import Toolbox

class GeometryTests: XCTestCase
{

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testNSSizeTimes()
    {
		let startSize = CGSize(width: 12, height: 24)
		let multiply = startSize * 2
		XCTAssert(multiply.width == 24)
		XCTAssert(multiply.height == 48)

		let divide = multiply / 3
		XCTAssert(divide.width == 8)
		XCTAssert(divide.height == 16)
    }

	func testPointTranslation()
	{
		let p = CGPoint(x: 1, y: 2)
		let s = CGSize(width: 1, height: 3)
		let p2 = p + s
		XCTAssert(p2.x == 2)
		XCTAssert(p2.y == 5)
	}

	func testNSSizeArithmetic()
	{
		let s1 = CGSize(width: 2, height: 3)
		let s2 = CGSize(width: 1, height: 2)
		let sum = s1 + s2
		XCTAssert(sum.width == 3)
		XCTAssert(sum.height == 5)
		let difference = s1 - s2
		XCTAssert(difference.width == 1)
		XCTAssert(difference.height == 1)
	}
}
