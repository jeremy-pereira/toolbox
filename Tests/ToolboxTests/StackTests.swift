//
//  StackTests.swift
//  SwiftHTTP
//
//  Created by Jeremy Pereira on 20/11/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class StackTests: XCTestCase
{
    var stack: Toolbox.Stack<Int>!

    override func setUp()
    {
        super.setUp()
        stack = Stack<Int>()
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSingleItem()
    {
		stack.push(1)
        stack.push(2)
        XCTAssert(stack.count == 2, "Wrong stack count")
        XCTAssert(stack.top == 2, "Invalid peek")
        XCTAssert(stack.pop() == 2, "Pop failed")
        XCTAssert(stack.pop() == 1, "Seconf pop failed")
		XCTAssert(stack.isEmpty, "Stack should be empty")
        XCTAssert(stack.pop() == nil, "Managed to op empty stack")
    }

    func testThrowingPop()
    {
        stack.push(1)
        do
        {
            let x = try stack.throwingPop()
            XCTAssert(x == 1)
        }
        catch
        {
            XCTFail("\(error)")
        }
        do
        {
            _ = try stack.throwingPop()
            XCTFail("Throwing pop on an empty stack should throw")
        }
        catch
        {
        }
    }

    func testPerformanceExample()
    {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
