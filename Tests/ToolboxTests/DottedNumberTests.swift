//
//  DottedNumberTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 22/09/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class DottedNumberTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDescription()
    {
        let aNumber = DottedNumber([1, 0, 1])
        XCTAssert(aNumber.description == "1.0.1", "Wrong description \(aNumber)")
    }

    func testEquals()
    {
        XCTAssert(DottedNumber([1, 0]) == DottedNumber([1, 0]))
        XCTAssert(DottedNumber([1, 1]) != DottedNumber([1, 0]))
        XCTAssert(DottedNumber([1, 1, 0]) == DottedNumber([1, 1]))
        do
        {
            let aNumber = try DottedNumber(ascii: "1.1.0".utf8)
            XCTAssert(aNumber == DottedNumber([1, 1]))
        }
        catch
        {
            XCTFail("\(error)")
        }
    }

    func testCompare()
    {
        XCTAssert(DottedNumber([1, 1]) < DottedNumber([1, 1, 1]))
        XCTAssert(DottedNumber([1, 0]) >= DottedNumber([1, 0]))
    }

}
