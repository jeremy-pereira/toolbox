//
//  LoggerTests.swift
//  LoggerTests
//
//  Created by Jeremy Pereira on 23/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class LoggerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNameComponents()
    {
        XCTAssert("foo.bar".withoutLastComponent == "foo", "Failed foo.bar")
        XCTAssert("foo".withoutLastComponent == "", "Failed foo")
        XCTAssert("foo.bar.baz".withoutLastComponent == "foo.bar", "Failed foo.bar.baz")
        XCTAssert("".withoutLastComponent == "", "Failed \"\"")
    }

    func testLogger()
    {
		Logger.set(level: Logger.Level.info, forName: "")
		Logger.set(level: Logger.Level.debug, forName: "foo")
		XCTAssert(Logger.getLogger("").level == Logger.Level.info, "Root logger has wrong level")
        XCTAssert(Logger.getLogger("foo").level == Logger.Level.debug, "foo logger has wrong level")
        let fooBarLogger = Logger.getLogger("foo.bar")
        XCTAssert(fooBarLogger.level == Logger.Level.debug, "foo.bar logger has wrong level")
		Logger.set(level: Logger.Level.info, forName: "foo.bar")
        XCTAssert(fooBarLogger.level == Logger.Level.info, "foo.bar logger has wrong level")
    }

	func testLog()
	{
		let log = Logger.getLogger("LoggerTests.testLog")
		log.level = .debug
		log.info("line1")
		log.info("line2")
		log.outputStreams = [Array<String>()]
		log.debug("DEBUG YES")
		log.level = .info
		log.debug("DEBUG NO")
		log.info("DEBUG YES")
		let logMessages = log.outputStreams[0] as! Array<String>
		XCTAssert(logMessages.count == 2, "log messages got wrong count: \(logMessages)")
	}

	func testWith()
	{
		var log = Logger.getLogger("LoggerTests.testLog").with(level: .debug)
		log.info("line1")
		log.info("line2")
		log.outputStreams = [Array<String>()]
		log.debug("DEBUG YES")
		log = Logger.getLogger("LoggerTests.testLog").with(level: .info)
		log.debug("DEBUG NO")
		log.info("DEBUG YES")
		let logMessages = log.outputStreams[0] as! Array<String>
		XCTAssert(logMessages.count == 2, "log messages got wrong count: \(logMessages)")
	}

	func testLogStream()
	{
		let log = Logger.getLogger("LoggerTests.testLog")
		log.level = .debug
		log.outputStreams = [Array<String>()]
		print("DEBUG YES", to: &log.debugStream)
		log.level = .info
		print("DEBUG NO", to: &log.debugStream)
		print("INFO YES", to: &log.infoStream)
		let logMessages = log.outputStreams[0] as! Array<String>
		XCTAssert(logMessages.count == 2, "log messages got wrong count: \(logMessages)")
	}
}

extension Array: @retroactive TextOutputStream where Element == String
{
	public mutating func write(_ string: String)
	{
		if string != "" && string != "\n"
		{
			self.append(string)
		}
	}
}
