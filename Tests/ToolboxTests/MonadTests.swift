//
//  MonadTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 28/08/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class MonadTests: XCTestCase
{

    func testOptionalUnit()
    {
		if let x = Optional.unit(1)
        {
            XCTAssert(x == 1)
        }
        else
        {
            XCTFail("unit(1) is nil")
        }
    }

    private func div2(_ x: Int) -> Int?
    {
        return x % 2 == 0 ? x / 2 : nil
    }

    private func div3(_ x: Int) -> Int?
    {
        return x % 3 == 0 ? x / 3 : nil
    }

    private func div5(_ x: Int) -> Int?
    {
        return x % 5 == 0 ? x / 5 : nil
    }

    func testBind()
    {
		let result = Optional(30) >>- div2 >>- div3 >>- div5
        XCTAssert(result != nil && result! == 1)
    }

    func testBindWithNil()
    {
        let result = Optional(10) >>- div2 >>- div3 >>- div5
        XCTAssert(result == nil)
    }

    enum Fruit { case apple, pear, banana, cumquat }
    enum Car { case ford, jaguar, tesla }
    enum Band { case beatles, rollingStones, pinkFloyd }

    private func fruitToCar(_ f: Fruit) -> Car?
    {
        switch f
        {
        case .apple: return .jaguar
        case .pear: return .ford
        case .banana: return .tesla
        default: return nil
        }
    }

    private func carToBand(c: Car) -> Band?
    {
        switch c
        {
        case .jaguar: return .pinkFloyd
        default: return nil
        }
    }

    func testBindThreeTypes()
    {
		let band = Optional(Fruit.apple) >>- fruitToCar >>- carToBand
        XCTAssert(band != nil && band! == .pinkFloyd)
        let band2 = Optional(Fruit.pear) >>- fruitToCar >>- carToBand
        XCTAssert(band2 == nil)
    }

    func testAssociativity()
    {
        let band = Optional(Fruit.apple) >>- { x in fruitToCar(x) >>- carToBand }
        XCTAssert(band != nil && band! == .pinkFloyd)

    }
}
