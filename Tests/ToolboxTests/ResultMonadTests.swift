//
//  ResultMonadTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 04/05/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox


class ResultMonadTests: XCTestCase
{

	func testResultUnit()
	{
		let wrapped = Result<Int, ResultMonadTests.Error>.unit(1)
		if case Result.success(let x) = wrapped
		{
			XCTAssert(x == 1)
		}
		else
		{
			XCTFail("unit(1) is \(wrapped)")
		}
	}

	private func div2(_ x: Int) -> Result<Int, Error>
	{
		return x % 2 == 0 ? Result.success(x / 2) : Result.failure(.anError)
	}

	private func div3(_ x: Int) -> Result<Int, Error>
	{
		return x % 3 == 0 ? Result.success(x / 3) : Result.failure(.anError)
	}

	private func div5(_ x: Int) -> Result<Int, Error>
	{
		return x % 5 == 0 ? Result.success(x / 5) : Result.failure(.anError)
	}

	func testBind()
	{
		let result = Result.unit(30) >>- div2 >>- div3 >>- div5
		switch result
		{
		case .failure(let reason):
			XCTFail("\(reason)")
		case .success(let result):
			XCTAssert(result == 1)
		}
	}

	func testBindWithNil()
	{
		let result = Result.unit(10) >>- div2 >>- div3 >>- div5
		switch result
		{
		case .failure:
			break
		case .success:
			XCTFail("\(result)")
		}
	}

	enum Fruit { case apple, pear, banana, cumquat }
	enum Car { case ford, jaguar, tesla }
	enum Band { case beatles, rollingStones, pinkFloyd }

	private func fruitToCar(_ f: Fruit) -> Result<Car, Error>
	{
		switch f
		{
		case .apple: return Result.success(.jaguar)
		case .pear: return Result.success(.ford)
		case .banana: return Result.success(.tesla)
		default: return .failure(.anError)
		}
	}

	private func carToBand(c: Car) -> Result<Band, Error>
	{
		switch c
		{
		case .jaguar: return Result.success(.pinkFloyd)
		default: return .failure(.anError)
		}
	}

	func testBindThreeTypes()
	{
		let band = Result.unit(Fruit.apple) >>- fruitToCar >>- carToBand
		switch band
		{
		case .failure:
			XCTFail("\(band)")
		case .success(let b):
			XCTAssert(b == .pinkFloyd)
		}
		let band2 = Result.unit(Fruit.pear) >>- fruitToCar >>- carToBand
		switch band2
		{
		case .failure:
			break
		case .success:
			XCTFail("\(band2)")
		}
	}

	func testAssociativity()
	{
		let band = Result.unit(Fruit.apple) >>- { x in fruitToCar(x) >>- carToBand }
		switch band
		{
		case .failure:
			XCTFail("\(band)")
		case .success(let b):
			XCTAssert(b == .pinkFloyd)
		}
	}
}

extension ResultMonadTests
{
	enum Error: Swift.Error
	{
		case anError
	}
}
