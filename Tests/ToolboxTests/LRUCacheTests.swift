//
//  LRUCacheTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 11/06/2018.
//  Copyright © 2018 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class LRUCacheTests: XCTestCase
{
    enum Err: Error
    {
        case sign
    }

	private let backingValues =
        [ "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]


    override func setUp()
    {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    private func createCache() -> LRUCache<Int, String>
    {
        return LRUCache(limit: 3)
        {
            (key: Int) -> String? in
            guard key >= 0 else { throw Err.sign }

            if key < self.backingValues.count
            {
                return self.backingValues[key]
            }
            else
            {
                return nil
            }
        }
    }

    func testLookup()
    {
        let cache = createCache()
        do
        {
            let value = try cache.lookup(2)
			XCTAssert(value == "two")
            XCTAssert(cache.misses == 1)
            XCTAssert(cache.lookups == 1)
            let value2 = try cache.lookup(2)
            XCTAssert(value2 == "two")
            XCTAssert(cache.misses == 1)
            XCTAssert(cache.lookups == 2)
        }
        catch
        {
            XCTFail("Error \(error)")
        }
    }

    func testInvalidate()
    {
        let cache = createCache()
        do
        {
            let value = try cache.lookup(2)
            XCTAssert(value == "two")
            XCTAssert(cache.misses == 1)
            XCTAssert(cache.lookups == 1)
            cache.invalidate()
            let value2 = try cache.lookup(2)
            XCTAssert(value2 == "two")
            XCTAssert(cache.misses == 2)
            XCTAssert(cache.lookups == 2)
        }
        catch
        {
            XCTFail("Error \(error)")
        }
    }

    func testMissMisses()
    {
        let cache = createCache()
        do
        {
            let value = try cache.lookup(10)
            XCTAssert(value == nil)
            XCTAssert(cache.misses == 1)
            XCTAssert(cache.lookups == 1)
        }
        catch
        {
            XCTFail("Error \(error)")
        }
    }


    func testErrorPropagates()
    {
        let cache = createCache()
        do
        {
            let _ = try cache.lookup(-1)
            XCTFail("Expected an error to be thrown")
        }
        catch (Err.sign)
        {
            // pass
        }
        catch
        {
            XCTFail("Error \(error)")
        }
    }

    func testcacheDiscards()
    {
        let cache = createCache()
        do
        {
            let _ = try cache.lookup(0)
            let _ = try cache.lookup(1)
            let _ = try cache.lookup(2)
            // LRU order 2 1 0
            let _ = try cache.lookup(3)
            // LRU order 3 2 1
           	XCTAssert(cache.misses == 4)
            XCTAssert(cache.lookups == 4)
            let _ = try cache.lookup(1)
            // LRU order 1 3 2
            XCTAssert(cache.misses == 4)
            XCTAssert(cache.lookups == 5)
            let _ = try cache.lookup(0)
            // LRU order 0 1 3
            XCTAssert(cache.misses == 5)
            XCTAssert(cache.lookups == 6)
            let _ = try cache.lookup(1)
            // LRU order 1 0 3
            XCTAssert(cache.misses == 5)
            XCTAssert(cache.lookups == 7)
       }
        catch
        {
            XCTFail("Error \(error)")
        }

    }
}
