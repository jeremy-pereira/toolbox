//
//  ToolboxTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 28/09/2015.
//  Copyright © 2015 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class ToolboxTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUInt16()
    {
        let testData: UInt16 = 0xfefd
        XCTAssert(testData.hexString == "fefd", "Invalid hax string\(testData.hexString)")
        XCTAssert(testData.lowByte == 0xfd, "Invalid low byte")
        XCTAssert(testData.highByte == 0xfe, "Invalid high byte")

        let testWord = UInt16(low: 1, high: 2)
		XCTAssert(testWord == 0x201, "Test word not constructed correctly \(testWord)")
    }

	func testArrayDictionary()
	{
		var dictionary = [ 1 : [1, 2, 3]]
		dictionary.append(value: 4, for: 1)
		dictionary.append(value: 3, for: 2)
		XCTAssert(dictionary[1]?.count ?? -1 == 4)
		XCTAssert(dictionary[2]?.count ?? -1 == 1)
	}

	func testReduceWithStop()
	{
		let s = [1, 2, 3, 4]
		let (allResults, didAll) = s.reduceWithStop(into: 0)
		{
			(acc, el, _) in
			acc += el
		}
		XCTAssert(didAll == true)
		XCTAssert(allResults == 10)
		let (someResults, didSome) = s.reduceWithStop(into: 0)
		{
			(acc, el, stop) in
			acc += el
			stop = el == 2
		}
		XCTAssert(!didSome)
		XCTAssert(someResults == 3)
	}
}
