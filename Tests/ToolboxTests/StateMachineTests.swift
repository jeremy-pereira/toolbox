//
//  StateMachineTests.swift
//  Toolbox
//
//  Created by Jeremy Pereira on 21/06/2016.
//  Copyright © 2016 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import Toolbox

class StateMachineTests: XCTestCase
{

    enum TestState: StateType
    {
        case start, zero, one, result00, result01, result10

        func canTransition(to: TestState) -> TransitionShould<TestState>
        {
            switch (self, to)
            {
            case (.start, .zero):
                return .carryOn
            case (.start, .one):
                return .carryOn
            case (.zero,  .result00):
                return  .redirect(.start)
            case (.zero,  .result01):
                return .redirect(.start)
            case (.one,  .result10):
                return  .redirect(.start)
            case (.one,  .result01):
                return  .redirect(.start)
            default:
                return .abort
            }
        }
    }

    var stateMachine: StateMachine<TestState>!
    override func setUp()
    {
        super.setUp()
        stateMachine = StateMachine(startState: TestState.start)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSuccessfulTransition()
    {
        stateMachine.onFailure = { from, to in XCTFail("\(from) => \(to)") }

        var actionCalled = false
        stateMachine.transition(to: .zero)
        {
            from, to in
            print("\(from) \(to)")
            actionCalled = true
        }
        XCTAssert(actionCalled, "The action wasn't called")
    }

    func testTransitionNoAction()
    {
        stateMachine.onFailure = { from, to in XCTFail("\(from) => \(to)") }

        stateMachine.transition(to: .zero)
        XCTAssert(stateMachine.state == .zero, "Invalid state \(stateMachine.state)")
    }

    func testRedirect()
    {
        stateMachine.onFailure = { from, to in XCTFail("\(from) => \(to)") }

        var actionCalled = false
        stateMachine.transition(to: .zero)
        {
            from, to in
            print("\(from) \(to)")
            actionCalled = true
        }
        XCTAssert(actionCalled, "The action wasn't called")
        actionCalled = false
        stateMachine.transition(to: .result01)
        {
            from, to in
            print("\(from) \(to)")
            actionCalled = true
        }
        XCTAssert(stateMachine.state == .start)
    }

    func testUnsuccessfulTransition()
    {
        var failureCalled = false
        stateMachine.onFailure = { from, to in failureCalled = true }

        stateMachine.transition(to: .result00)
        {
            from, to in
            XCTFail("\(from) => \(to) Should not run the action")
        }
        XCTAssert(failureCalled, "The action wasn't called")
    }

    func testNestedTransition()
    {
        var failureCalled = false
        stateMachine.nestedTransitionError = { _ in failureCalled = true }

        stateMachine.transition(to: .zero)
        {
            from, to in
            stateMachine.transition(to: .result01)
        }
        XCTAssert(failureCalled, "The nested transition handler wasn't called")
    }

    func testNestedTransitionAgain()
    {
        var failureCalled = false
        stateMachine.nestedTransitionError = { _ in failureCalled = true }

        stateMachine.transition(to: .zero)
        {
            from, to in
            stateMachine.transition(to: .result01) { _, _ in }
        }
        XCTAssert(failureCalled, "The nested transition handler wasn't called")
    }

}
