//
//  ShuntingYardTests.swift
//  ToolboxTests
//
//  Created by Jeremy Pereira on 03/02/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
import Toolbox

private let log = Logger.getLogger("ToolboxTests.ShuntingYardTests")

fileprivate enum Token: CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
        case .number(let num):
            return "\(num)"
        case .op(let symbol):
            return "\(symbol)"
        case .prefix(let symbol):
            return "\(symbol)"
        case .lPar:
            return "("
        case .rPar:
            return ")"
        }
    }

    case op(String)
    case prefix(String)
    case number(Int)
    case lPar
    case rPar

    var precedence: Int
    {
        switch self
        {
        case .op(let symbol):
            return Token.precedences[symbol] ?? -1
        case .prefix:
            return Int.max
        default:
            return -1
        }
    }
    public var operation: (inout Stack<Int>) -> ()
    {
        switch self
        {
        case .op(let symbol):
            return Token.operations[symbol]!
        case .prefix(let symbol):
            return Token.prefixOperations[symbol]!
        case .number(let value):
            return { $0.push(value) }
        case .lPar, .rPar:
            fatalError("Parentheses do not have operations")
        }
    }
    static let precedences = [ "+" : 10, "-" : 10, "*" : 20, "/" : 20, "+<" : 10, "-<" : 10 ]
    public static let operations: [String : (inout Stack<Int>) -> ()] =
    [
        "+" : { $0.push($0.pop()! + $0.pop()!) },
        "-" : { let d = $0 .pop()! ; return $0.push($0.pop()! - d) },
        "*" : { $0.push($0.pop()! * $0.pop()!) },
        "/" : { let d = $0 .pop()! ; return $0.push($0.pop()! / d) },
        "+<" : { $0.push($0.pop()! + $0.pop()!) },
        "-<" : { let d = $0 .pop()! ; return $0.push($0.pop()! - d) },
	]

    public static let prefixOperations: [String : (inout Stack<Int>) -> ()] =
    [
        "-" : { $0.push(-$0.pop()!) }
    ]
    public static let rightAssociative: Set<String> = [ "+<", "-<"]

}

extension Token: Shuntable
{
    var isLeftAssociative: Bool
    {
        switch self
        {
        case .op(let string):
            return !Token.rightAssociative.contains(string)
        default:
            return false
        }
    }


    var isOperator: Bool
    {
        switch self
        {
        case .op, .prefix:
            return true
        default:
            return false
        }
    }

    var isLPar: Bool
    {
        switch self
        {
        case .lPar:
            return true
        default:
            return false
        }
    }

    var isRPar: Bool
    {
        switch self
        {
        case .rPar:
            return true
        default:
            return false
        }
    }

    var isOperand: Bool
    {
        switch self
        {
        case .number:
            return true
        default:
            return false
        }
    }
}

class ShuntingYardTests: XCTestCase
{

    override func setUp()
    {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSimpleExpression()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }

        let input: [Token] = [ .number(2), .op("+"), .number(6), .op("/"), .number(3), .op("*"), .number(4), .op("-"), .number(8)]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")
            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == 2)

        }
        catch
        {
            XCTFail("\(error)")
        }
    }


    func testParentheses()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }

        let input: [Token] = [ .lPar, .number(2), .op("+"), .number(6), .op("/"), .number(3), .rPar, .op("*"), .number(4), .op("-"), .number(8)]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")

            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == 8, "Unexpected result \(result)")
        }
        catch
        {
            XCTFail("\(error)")
        }
    }

    func testLeftAssociativity()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }
		let input: [Token] = [.number(1), .op("-"), .number(2), .op("-"), .number(3) ]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")

            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == -4, "Unexpected result \(result)")
        }
        catch
        {
            XCTFail("\(error)")
        }
    }

    func testRightAssociativity()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }
       let input: [Token] = [.number(1), .op("-<"), .number(2), .op("-<"), .number(3) ]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")

            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == 2, "Unexpected result \(result)")
        }
        catch
        {
            XCTFail("\(error)")
        }
    }


    func testPrefix()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }
        let input: [Token] = [.prefix("-"), .number(1), .op("+"), .number(2), .op("+"), .number(3) ]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")

            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == 4, "Unexpected result \(result)")
        }
        catch
        {
            XCTFail("\(error)")
        }
    }

    func testPrefixParentheses()
    {
        log.pushLevel(.debug) ; defer { log.popLevel() }
        Logger.pushLevel(.debug, forName: "Toolbox.ShuntingYard") ; defer { Logger.popLevel(forName: "Toolbox.ShuntingYard") }
        let input: [Token] = [.prefix("-"), .lPar, .number(1), .op("+"), .number(2), .rPar, .op("+"), .number(3) ]
        do
        {
            let output = try ShuntingYard<Token>().shunt(expression: input)
            log.debug("Output: \(output)")

            var dataStack = Stack<Int>()

            for token in output
            {
                token.operation(&dataStack)
            }
            guard let result = dataStack.pop() else { throw Error.emptyStack }
            XCTAssert(result == 0, "Unexpected result \(result)")
        }
        catch
        {
            XCTFail("\(error)")
        }
    }



//    func testPerformanceExample()
//    {
//        // This is an example of a performance test case.
//        self.measure
//        {
//            // Put the code you want to measure the time of here.
//        }
//    }
}

extension ShuntingYardTests
{
    fileprivate enum Error: Swift.Error
    {
		case emptyStack
    }
}
