// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "Toolbox",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        .library(name: "Toolbox", targets: ["Toolbox"]),
    ],
    dependencies: [
  //      .package(url: "https://url/of/another/package/named/Utility", from: "1.0.0"),
		.package(url: "https://github.com/apple/swift-system", from: "1.3.2")
    ],
    targets: [
        .target(name: "Toolbox", dependencies: [.product(name: "SystemPackage", package: "swift-system"),], path: "Toolbox"),
        .testTarget(name: "ToolboxTests", dependencies: ["Toolbox"]),
    ]
)
